import sys 
# to run this, need to specify: export PYTHONPATH=~/repos/bpodautopy/bpodautopy
sys.path.append('~/repos/bpodautopy/bpodautopy')

import numpy as np
import pandas as pd
import streamlit as st
from datetime import date
import manage_animals as ma
import helper_funcs_gui as hpf


st.title("Duan-Erlich Lab Animal Management Dashboard")
st.write("Welcome to the animal management GUI, the first page is for initializing new animals, please see left side bar for more functions")

st.header('Create New Animal')
st.write("DOB - Date of Born, Order Date: date you put order for this animals, or just the same as DOB if the animals were born here")
if 'animal_info' not in st.session_state:
    st.session_state.animal_info = {'subjid': ['JLI-R-0001'], 'pyratid': ['PAA-0000001'],'species': ['rat'],'gender':['F'],'ppl': ['PD867676F'],'strain':['LH'],'DOB':[date.today()],'date_ordered':[date.today()]}

try:
    userinfo = ma.experList() #just query something to check whether db connection is still alive
    users = {}
    for i in enumerate(userinfo):
        users[i[1][1]] = i[1][0]
except TypeError:
    st.write('db lost, reconnecting')
    ma.reconnect()
finally:
    if 'userinfo' not in st.session_state:
        st.session_state.userinfo = ma.experList()
    users = {}
    for i in enumerate(st.session_state.userinfo):
        users[i[1][1]] = i[1][0]
    userlist = users.keys()

    if 'all_subj' not in st.session_state:
        all_animal_list = list(ma.get_all_alive_animal_list())
        all_animal_list.insert(0, "")
        st.session_state.all_subj = all_animal_list

    if 'all_running_subj' not in st.session_state:
        running_animal_list = list(ma.get_all_running_animal_list())
        running_animal_list.insert(0,"")
        st.session_state.all_running_subj = running_animal_list


if 'confirmed_upload' not in st.session_state:
    st.session_state.confirmed_upload = False

if 'subjid_checks_flag' not in st.session_state:
    st.session_state.subjid_checks_flag = False

form = st.form(key='animal_information')
col1, col2, col3 = form.columns(3)

subjid_to_preview = st.session_state.animal_info['subjid'][0]
subjid_to_preview = subjid_to_preview.split(",")
if isinstance(subjid_to_preview,list):
    subjid_to_preview = subjid_to_preview[0]

subjid = col1.text_input(label='subjid',value=subjid_to_preview)
pyratid = col1.text_input(label='pyratid',value=st.session_state.animal_info['pyratid'][0])
date_ordered = col1.date_input(label='ordered_date',value=st.session_state.animal_info['date_ordered'][0])

gender = col2.selectbox(label='gender',options=('F', 'M'))
species = col2.selectbox(label='species',options=('rat', 'mouse', 'tester'))
ppl = col2.selectbox(label='PPL',options=('PD867676F', 'PE4FA53CB', 'XXXXXXXXX'))

strain = col3.selectbox(label='strain',options=('SD', 'LH', 'LE','BN','CD1','C57B6'))
order_dob = col3.date_input(label='DOB',value=st.session_state.animal_info['DOB'][0])
owner = col3.selectbox(label='exper',options=list(userlist))

submit_button = form.form_submit_button(label='Submit')

if submit_button:
    experid =  st.session_state.userinfo[list(userlist).index(owner)][0]
    d = {'subjid': [subjid], 'pyratid': [pyratid],'species': [species],'gender':[gender],'ppl': [ppl],'strain':[strain],'DOB':[order_dob],'date_ordered':[date_ordered],'experid':[experid]}
    st.session_state.animal_info = d
    st.session_state.subjid_checks_flag = ma.check_subject_id_format(d['subjid'][0])
    df = pd.DataFrame(data=d)
    st.table(df)
    st.session_state.confirmed_upload = True


if st.session_state.subjid_checks_flag and st.session_state.confirmed_upload:
    if st.button('Confirm Upload?'):
        ma.create_one_animal(st.session_state.animal_info,st.session_state.animal_info['experid'][0],order_on=st.session_state.animal_info['date_ordered'][0].strftime('%Y-%m-%d %H:%M:%S'),arrived_age=None)
        st.write('uploaded')
        st.session_state.confirmed_upload = False
elif st.session_state.confirmed_upload and not st.session_state.subjid_checks_flag:
    st.write('!!! Please check the subjid format and update it to the format AAA-A-0000 !!')



# assign animals
st.header('Assign Animal Ownership')
st.caption('The subjid must follow the format XXX-X-0000. e.g. JLI-R-0001')
st.caption('you can also put a list of animals, for example: JLI-R-0001,JLI-R-0002,JLI-R-0003,JLI-R-0004')
form4 = st.form(key='assign_animals',clear_on_submit=True)
col1_4, col2_4= form4.columns(2)

subjid_exper = col1_4.text_input(label='subjid/list',value='')
exper_exper = col2_4.selectbox(label='exper',options=list(userlist))


submit_exper_button = form4.form_submit_button(label='Submit')

if submit_exper_button:
    st.session_state.animal_info['subjid'][0] = subjid_exper.strip()
    subjid_exper = subjid_exper.split(",")
    assert ma.check_subject_id_format(subjid_exper), "Subject ID must be in the format AAA-A-0000"
    experid =  st.session_state.userinfo[list(userlist).index(exper_exper)][0]
    ma.assign_to_experimenter(subjid_exper, exper_exper)
    st.write('submitted!')

# Mark arrived animals
st.header('Mark Animals Arrival')
st.write("stattime: the time we received this animals from the vendor, or the day we get this animal from B2")
form_arrived = st.form(key='animal arrive',clear_on_submit=True)
col1_arrive, col2_arrive = form_arrived.columns(2)
subjid_arrive = col1_arrive.text_input(label='subjid/list',value='')
exper_arrive = col1_arrive.selectbox(label='exper',options=list(userlist))

stattime_arrive = col2_arrive.text_input(label='stattime(y-m-d h:m:s null for now)',value='')

submit_arrival_button = form_arrived.form_submit_button(label='Submit')
if submit_arrival_button:
    if hpf.validate(stattime_arrive) or stattime_arrive == '':
        if stattime_arrive == '':
            stattime_arrive = None
        subjid_arrive = subjid_arrive.split(",")
        assert ma.check_subject_id_format(subjid_arrive), "Subject ID must be in the format AAA-A-0000"
        experid =  st.session_state.userinfo[list(userlist).index(exper_arrive)][0]
        ma.change_animal_status(subjid=subjid_arrive,status='arrived', experid=int(experid), stattime=stattime_arrive)
        ma.change_animal_water_status(subjid=subjid_arrive,status='free', experid=int(experid), stattime=stattime_arrive)
        ma.change_animal_health_status(subjid=subjid_arrive, status='good', experid=int(experid), by_rfid=False, stattime=stattime_arrive)
        st.write('submitted!')
    else:
        st.write('incorrect date format!')

# create animal cage
st.header('Create new cage for animals')
st.caption('Only use this if you are creating new cage with a list of animals input together')
st.caption('If you are moving animals to a cage that already exist in the system, use move_cage in the second tab')
form5 = st.form(key='create_cage',clear_on_submit=True)
col1_5, col2_5 = form5.columns(2)

subjid_cage = col1_5.text_input(label='subjid/list',value='')
exper_cage = col1_5.selectbox(label='exper',options=list(userlist))

pyratcageid_cage = col2_5.text_input(label='pyrat cage id',value='')
stattime_cage = col2_5.text_input(label='stattime(y-m-d h:m:s null for now)',value='')

submit_cage_button = form5.form_submit_button(label='Submit')

if submit_cage_button:
    if hpf.validate(stattime_cage) or stattime_cage == '':
        if stattime_cage == '':
            stattime_cage = None
        st.session_state.animal_info['subjid'][0] = subjid_cage
        subjid_cage = subjid_cage.split(",")
        assert ma.check_subject_id_format(subjid_cage), "Subject ID must be in the format AAA-A-0000"
        experid =  st.session_state.userinfo[list(userlist).index(exper_cage)][0]
        db_cageid = ma.create_cage(subjlist=subjid_cage,experid=int(experid), pyratcageid = pyratcageid_cage,stattime=stattime_cage)
        st.write('submitted!, cageid: '+ str(db_cageid))
    else:
        st.write('incorrect date format!')

# assign animal RFID
st.header('Assign animal RFID')
form7 = st.form(key='tag_animals',clear_on_submit=True)
col1_7, col2_7= form7.columns(2)

subjid_tagging = col1_7.text_input(label='subjid',value='')
exper_tagging = col1_7.selectbox(label='exper',options=list(userlist))

rfid_tagging = col2_7.text_input(label='RFID(please use RFID scanner to input)',value='')


submit_rfid_button = form7.form_submit_button(label='Submit')

if submit_rfid_button and len(rfid_tagging)>14:
    assert ma.check_subject_id_format(subjid_tagging), "Subject ID must be in the format AAA-A-0000"
    st.session_state.animal_info['subjid'][0] = subjid_tagging
    experid =  st.session_state.userinfo[list(userlist).index(exper_tagging)][0]
    ma.tag_animal(subjid_tagging, rfid_tagging)
    ma.change_animal_status(subjid=subjid_tagging,status='free', experid=int(experid), stattime=None)
    st.write('submitted!')
