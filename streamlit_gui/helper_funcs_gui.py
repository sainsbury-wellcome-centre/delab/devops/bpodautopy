from datetime import datetime

def validate(date_text):
    try:
        if date_text != datetime.strptime(date_text, "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S"):
            raise ValueError
        return True
    except ValueError:
        return False