import numpy as np
import pandas as pd
import streamlit as st
from datetime import date
import manage_animals as ma

st.title("Manual Uploading (backup functions)")
st.write("Please go back to the home page and refresh if db connection is lost")

users = {}
for i in enumerate(st.session_state.userinfo):
    users[i[1][1]] = i[1][0]
    userlist = users.keys()

# manually upload weight
st.header('Upload Weight')
form_bw = st.form(key='manually upload weight',clear_on_submit=True)
col1_bw, col2_bw= form_bw.columns(2)

rfid_bw = col1_bw.text_input(label='RFID(please use RFID scanner to input)',value='')
bw = col2_bw.number_input('Body Weight')
exper_bw = col1_bw.selectbox(label='exper',options=list(userlist))

submit_bw_button = form_bw.form_submit_button(label='Submit')

if submit_bw_button:
    animal_info = ma.getAnimalInfo(rfid_bw)
    if animal_info:
        subjid = animal_info[0][0]
        experid =  st.session_state.userinfo[list(userlist).index(exper_bw)][0]
        ma.massToDB(bw, subjid, experid)
        st.write(f'BW for {subjid} submitted!: {bw} g')
    else:
        st.write('Wrong RFID, Please Check!')