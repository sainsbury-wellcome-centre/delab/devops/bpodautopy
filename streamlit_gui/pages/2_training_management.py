import numpy as np
import pandas as pd
import streamlit as st
from datetime import date
import manage_animals as ma

st.title("Training Management")
st.write("Please go back to the home page and refresh if db connection is lost")

users = {}
for i in enumerate(st.session_state.userinfo):
    users[i[1][1]] = i[1][0]
    userlist = users.keys()


# update animal expgroup
st.header('Assign/Update Animal expgoup')
form9 = st.form(key='expgroup_animals',clear_on_submit=True)
col1_9, col2_9= form9.columns(2)

subjid_expgroup = col1_9.selectbox(label='subjid',options=st.session_state.all_running_subj)
expgroup = col2_9.text_input(label='expgroup',value='')


submit_expgroup_button = form9.form_submit_button(label='Submit')

if submit_expgroup_button and subjid_expgroup == "":
    st.write('Please input a Subjid')
elif submit_expgroup_button:
    st.session_state.animal_info['subjid'][0] = subjid_expgroup
    subjid_expgroup = subjid_expgroup.split(",")
    ma.assign_expgroup(subjlist=subjid_expgroup, expgroupid=int(expgroup))
    st.write('submitted!')

# update animal training stage
st.header('Update Animal training stage')
form10 = st.form(key='stage_animals',clear_on_submit=True)
col1_10, col2_10= form10.columns(2)

subjid_stage = col1_10.selectbox(label='subjid',options=st.session_state.all_running_subj)
new_stage = col1_10.text_input(label='stage',value='')

exper_stage = col2_10.selectbox(label='exper',options=list(userlist))
clear_setting_data = col2_10.checkbox(label='clear settings data?',value=False)

submit_stage_button = form10.form_submit_button(label='Submit')

if submit_stage_button and subjid_stage == "":
    st.write('Please input a Subjid')
elif submit_stage_button:
    st.session_state.animal_info['subjid'][0] = subjid_stage
    subjid_stage = subjid_stage.split(",")
    experid =  st.session_state.userinfo[list(userlist).index(exper_stage)][0]
    ma.set_subject_stage(subjlist=subjid_stage, stage=int(new_stage), experid=int(experid), clear_subject_settings=clear_setting_data)
    st.write('submitted!')

# retire animals
st.header('Retire Animals (remove from training)')
form_retire = st.form(key='retire_animald',clear_on_submit=True)
col1_retire, col2_retire = form_retire.columns(2)
subjid_retire = col1_retire.selectbox(label='subjid/list',options=st.session_state.all_running_subj)
exper_retire = col1_retire.selectbox(label='exper',options=list(userlist))

water_retire = col2_retire.checkbox(label='Remove Water Restriction?',value=False)

submit_retire_button = form_retire.form_submit_button(label='Submit')
if submit_retire_button and subjid_retire == "":
    st.write('Please input an animal!')
elif submit_retire_button:
    experid =  st.session_state.userinfo[list(userlist).index(exper_retire)][0]
    ma.retire_animal(subjid=subjid_retire,water=water_retire,experid=experid)
    st.write('submitted!')