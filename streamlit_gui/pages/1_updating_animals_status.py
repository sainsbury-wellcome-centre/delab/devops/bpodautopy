import numpy as np
import pandas as pd
import streamlit as st
from datetime import date
import helper_funcs_gui as hpf
import manage_animals as ma

st.title("Updating Animal Status")
st.write("Please go back to the home page and refresh if db connection is lost")

users = {}
for i in enumerate(st.session_state.userinfo):
    users[i[1][1]] = i[1][0]
    userlist = users.keys()

try:
    userinfo = ma.experList() #just query something to check whether db connection is still alive
    users = {}
    for i in enumerate(userinfo):
        users[i[1][1]] = i[1][0]
except TypeError:
    st.write('db lost, reconnecting')
    ma.reconnect()
finally:
    if 'experinfo' not in st.session_state:
        st.session_state.userinfo = ma.experList()
        users = {}
        for i in enumerate(st.session_state.userinfo):
            users[i[1][1]] = i[1][0]
        userlist = users.keys()

    if 'all_subj' not in st.session_state:
        all_animal_list = list(ma.get_all_alive_animal_list())
        all_animal_list.insert(0, "")
        st.session_state.all_subj = all_animal_list

    if 'all_running_subj' not in st.session_state:
        running_animal_list = list(ma.get_all_running_animal_list())
        running_animal_list.insert(0,"")
        st.session_state.all_running_subj = running_animal_list

# put snimals to water restriction
st.header('Put animals to water restriction')
st.subheader('use this tab to put free animals to WR')
st.caption('procedure to put animals to water restriction:')
st.caption(' first, weight the animal with the scale-app to get the baseline before WR')
st.caption(' then put subjid or a list of subjid in this tab, uncheck the checkbox if you delibrately want to use previous baseline of this animal')
st.caption(' select your name, press OK')
st.caption(' DOUBLE CHECK on dash.deneuro.org/massview to see if you got your animals in place and the baseline is correct!!')
form_water_restriction = st.form(key='put water restriction',clear_on_submit=True)
col1_ws, col2_ws = form_water_restriction.columns(2)

subjid_ws = col1_ws.text_input(label='subjid/list format: AAA-A-0000',value='')
exper_ws = col1_ws.selectbox(label='exper',options=list(userlist))

update_baseline = col2_ws.checkbox(label='Update to lastest baseline?',value=True)
stattime_ws = col2_ws.text_input(label='stattime(y-m-d h:m:s null for now)',value='')

submit_ws_button = form_water_restriction.form_submit_button(label='Submit')

if submit_ws_button:
    if hpf.validate(stattime_ws) or stattime_ws == '':
        if stattime_ws == '':
            stattime_ws = None
        st.session_state.animal_info['subjid'][0] = subjid_ws
        subjid_wstatus = subjid_ws.split(",")
        assert ma.check_subject_id_format(subjid_wstatus), "Subject ID must be in the format AAA-A-0000"
        experid =  st.session_state.userinfo[list(userlist).index(exper_ws)][0]
        if update_baseline:
            message = ma.update_animal_baseline(subjid_wstatus)
        ma.change_animal_status(subjid=subjid_wstatus,status='ready', experid=int(experid), stattime=stattime_ws)
        ma.change_animal_water_status(subjid=subjid_wstatus,status='controlled', experid=int(experid), stattime=stattime_ws)
        st.write(message)
    else:
        st.write('incorrect date format!')

# animal status
st.header('Change Animal Status')
st.subheader('status for expreiment')
st.caption(' free - no experiment right not, ready - water/food restriction already, ready for expreiment, running - running experiment right now')
form2 = st.form(key='animal_status',clear_on_submit=True)
col1_s, col2_s = form2.columns(2)

subjid_status = col1_s.text_input(label='subjid/list format: AAA-A-0000',value='')
exper = col1_s.selectbox(label='exper',options=list(userlist))

setting_status = col2_s.selectbox(label='status',options=('arrived','free','ready','running','ordered'))
stattime = col2_s.text_input(label='stattime(y-m-d h:m:s null for now)',value='')

submit_status_button = form2.form_submit_button(label='Submit')

if submit_status_button:
    if hpf.validate(stattime) or stattime == '':
        if stattime == '':
            stattime = None
        st.session_state.animal_info['subjid'][0] = subjid_status
        subjid_status = subjid_status.split(",")
        assert ma.check_subject_id_format(subjid_status), "Subject ID must be in the format AAA-A-0000"
        experid =  st.session_state.userinfo[list(userlist).index(exper)][0]
        ma.change_animal_status(subjid=subjid_status,status=setting_status, experid=int(experid), stattime=stattime)
        st.write('submitted!')
    else:
        st.write('incorrect date format!')


# water status
st.header('Change Animal Watering Status')
st.subheader('status for water bottles')
st.caption('Please note that you may also need to set animal status to "free", if this animals is paused from experiment')
form3 = st.form(key='animal_water_status',clear_on_submit=True)
col1_3, col2_3 = form3.columns(2)

subjid_water = col1_3.text_input(label='subjid/list format: AAA-A-0000',value='')
exper_water = col1_3.selectbox(label='exper',options=list(userlist))

setting_water = col2_3.selectbox(label='water status',options=('free','controlled'))
stattime_water = col2_3.text_input(label='stattime(y-m-d h:m:s null for now)',value='')

submit_water_button = form3.form_submit_button(label='Submit')

if submit_water_button:
    if stattime_water == '':
        stattime_water = None
    st.session_state.animal_info['subjid'][0] = subjid_water
    experid =  st.session_state.userinfo[list(userlist).index(exper_water)][0]
    subjid_water = subjid_water.split(",")
    assert ma.check_subject_id_format(subjid_water), "Subject ID must be in the format AAA-A-0000"
    ma.change_animal_water_status(subjid=subjid_water,status=setting_water, experid=int(experid), stattime=stattime_water)
    st.write('submitted!')

# update animal health status
st.header('Update Animal Health Status')
form8 = st.form(key='animal_health_status',clear_on_submit=True)
col1_8, col2_8 = form8.columns(2)

subjid_health = col1_8.text_input(label='subjid/list format: AAA-A-0000',value='')
exper_health = col1_8.selectbox(label='exper',options=list(userlist))

status_health = col2_8.selectbox(label='status',options=('weight','good','clinical','recovery','surgery'))
stattime_health = col2_8.text_input(label='stattime(y-m-d h:m:s null for now)',value='')

submit_health_button = form8.form_submit_button(label='Submit')

if submit_health_button:
    if stattime_health == '':
        stattime_health = None
    st.session_state.animal_info['subjid'][0] = subjid_health
    subjid_health = subjid_health.split(",")
    assert ma.check_subject_id_format(subjid_health), "Subject ID must be in the format AAA-A-0000"
    experid =  st.session_state.userinfo[list(userlist).index(exper_health)][0]
    ma.change_animal_health_status(subjid=subjid_health, status=status_health, experid=int(experid), by_rfid=False, stattime=stattime_health)
    st.write('submitted!')

# sac animals
st.header('Sacrifice Animals')
form_sac = st.form(key='sac_animald',clear_on_submit=True)
col1_sac, col2_sac = form_sac.columns(2)
subjid_sac = col1_sac.selectbox(label='single subjid only',options=st.session_state.all_subj)
exper_sac = col1_sac.selectbox(label='exper',options=list(userlist))

comment_sac = col2_sac.text_input(label='comment and reason',value='')
stattime_sac = col2_sac.text_input(label='stattime(y-m-d h:m:s null for now)',value='')

submit_sac_button = form_sac.form_submit_button(label='Submit')
if submit_sac_button and subjid_sac == "":
    st.write('Please input an animal!')
elif submit_sac_button:
    if stattime_sac == '':
        stattime_sac = None
    experid =  st.session_state.userinfo[list(userlist).index(exper_sac)][0]
    ma.sac_subjid(subjid=subjid_sac,experid=experid,stattime=stattime_sac,cmt=comment_sac)
    st.write('submitted!')
    
# assign animal cage
st.header('Assign animals to existing cage')
form6 = st.form(key='assign_cage',clear_on_submit=True)
col1_6, col2_6 = form6.columns(2)

subjid_asscage = col1_6.text_input(label='subjid/list format: AAA-A-0000',value='')
pyratcageid_asscage = col1_6.text_input(label='pyrat cage id',value='')
cageid_asscage = col1_6.text_input(label='db cage id',value='')

exper_asscage = col2_6.selectbox(label='exper',options=list(userlist))
stattime_asscage = col2_6.text_input(label='stattime(y-m-d h:m:s null for now)',value='')

submit_asscage_button = form6.form_submit_button(label='Submit')

if submit_asscage_button:
    if stattime_asscage == '':
        stattime_asscage = None
    st.session_state.animal_info['subjid'][0] = subjid_asscage
    subjid_asscage = subjid_asscage.split(",")
    assert ma.check_subject_id_format(subjid_asscage), "Subject ID must be in the format AAA-A-0000"
    experid =  st.session_state.userinfo[list(userlist).index(exper_asscage)][0]
    db_cageid = ma.assign_to_cage(subjlist=subjid_asscage, pyratcageid = pyratcageid_asscage,cageid = int(cageid_asscage),experid=int(experid),stattime=stattime_asscage)
    st.write('submitted!')

# cage water status
st.header('Change Cage Watering Status')
form_cagewater = st.form(key='animal_cage_water_status',clear_on_submit=True)
col1_cagewater, col2_cagewater = form_cagewater.columns(2)

cageid_water = col1_cagewater.text_input(label='db cageid(leave empty if entered pyrat)',value='')
exper_cagewater = col1_cagewater.selectbox(label='exper',options=list(userlist))

pyratcageid_water = col2_cagewater.text_input(label='pyrat cageid(leave empty if entered db)',value='')
setting_cagewater = col2_cagewater.selectbox(label='cage water status',options=('waterin','waterout'))

submit_cagewater_button = form_cagewater.form_submit_button(label='Submit')
if submit_cagewater_button:
    experid =  st.session_state.userinfo[list(userlist).index(exper_cagewater)][0]
    if cageid_water == '':
       cageid_water = None
    else:
        pyratcageid_water = None
    
    ma.change_cage_water_status(cageid=cageid_water,pyratcageid=pyratcageid_water, status=setting_cagewater, experid=experid)
    st.write('submitted!')