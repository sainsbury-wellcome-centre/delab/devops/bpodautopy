# Bpod-auto Python Module   

This contains python code to help with managing rigs and animals. It is closely tied to the bpod-auto repository which is matlab code. This is setup a "monorepo" for all our python needs associated with our lab infrastructure. As such, there are multiple conda environments associated with this project.

## Connecting to the DB for analysis

There is a helper module `db.py` for connecting to the DB and working with pandas. You should have a file `~/.dbconf` with credentials for the database. Contact a lab member for help setting up this file.

These instructions assume you already have a working `conda`. If you do not, we recommend [miniconda](https://docs.anaconda.com/miniconda/miniconda-install/) or [miniforge](https://conda-forge.org/download/). 

Once you have a proper `~/.dbconf` file the following should work for connecting:

In the terminal
```
git clone https://gitlab.com/sainsbury-wellcome-centre/delab/devops/bpodautopy.git
cd bpodautopy
conda env create -f analysis.yml
export PYTHONPATH=`pwd`/bpodautopy
ipython
```

In ipython:
```
import db
import pandas
dbe = db.Engine()
pd.read_sql('explain met.animals', dbe)
```


## Scheduler

One of the main jobs of this project is to run scheduled tasks. The entry point for that is `bpodautopy/scheduler.py` which imports functionality from `bpodautopy/scheduling`. If you want to add something to the schedule, you should edit or add a module in `scheduling` and then import it into the `scheduler` and set the time when it should run. 

To run the scheduler, you generally need to use `env.yml` to setup the conda environment.

### One time setup
```
cd bpodautopy
conda create -f env.yml
```
### Running the scheduler
Open a tmux or zellij on a machine with a properly configured `~/.dbconf`
```
conda activate bpodauto
export PYTHONPATH=`pwd`/bpodautopy
python bpodautopy/scheduler.py
```

Ideally, the setup is done programmatically using ansible. 

## Managing Animals or Rigs

To manage animals or rigs use the `manage_animals.yml` to set up your conda environment. Check out the notebooks, e.g. `manage_animals.ipynb` to see how to use some of the functionality.

## Octagon Worker

Some of our hardware does not directly save data to the database, but instead emits ZMQ messages. These messages are processed by  `octagon_worker.py`. This should also have its own conda environment, but i think it currently shares the environment with the scheduler. TODO.

To run: 

Open a tmux or zellij on a machine with a properly configured `~/.dbconf`
```
conda activate bpodauto
export PYTHONPATH=`pwd`/bpodautopy
python bpodautopy/octagon_worker.py
```


