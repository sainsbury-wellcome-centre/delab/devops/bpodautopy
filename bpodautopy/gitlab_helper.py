"""
this file will have automatic helper function for gitlab. 
A pre-requisite to successfully use this file is to have a gitlab token
A personal or a project token will work.
"""
import requests
import json
import configparser as cp
from os import path

GITLAB_URL = "https://gitlab.com/api/v4"


def get_token_from_config():
    """
    this function will get the token from the config file
    :return: the token
    """
    
    config = cp.ConfigParser()
    ini_path = path.join(path.expanduser('~'),'.dbconf')
    config = cp.ConfigParser()
    config.sections()
    config.read(ini_path)

    token = config.get('gitlab', 'token')
    return token

def create_new_issue(token = None,
                     project_id = None,
                     title = None,
                     description= None, 
                     assignees = None):

    """
    this helper function will create a new issue in gitlab
    :param project_id: the project id where the issue will be created
    :param title: the title of the issue
    :param description: the description of the issue
    :param assignees: the assignees of the issue
    :return: the issue link
    """
    
    assert project_id is not None, "project_id is None"
    # create the url
    url = GITLAB_URL + "/projects/" + str(project_id) + "/issues"

    #get the token header
    # get this from the config file or from the user
    token = get_token_from_config() if token is None else token
    assert token is not None, "token is None"

    params = {
        "title": title,
        "description": description,
        "assignee_ids": assignees
    }

    headers = {
        "PRIVATE-TOKEN": token
    }

    # make the request
    response = requests.post(url, params=params, headers=headers)

    # check if the request was successful
    if response.status_code == 201:
        # get the issue id
        issue_link = json.loads(response.text)['web_url']
        issue_id = json.loads(response.text)['iid']
        return issue_link, issue_id
    else:
        # raise an error
        raise Exception("Error creating issue. Error code: " + str(response.status_code))



if __name__ == "__main__":
    # test the function
    token = get_token_from_config()
    project_id = 30093470
    title = "test issue"
    description = "this is a test issue from python"
    assignees = "12400350,3729571"
    issue_link, issue_id = create_new_issue(token, project_id, title, description, assignees)
    print(issue_link)
    print(issue_id)