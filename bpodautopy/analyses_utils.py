from datetime import date, timedelta
from dataclasses import dataclass, field, asdict
import pandas as pd
import warnings
import json
from copy import deepcopy
import numpy as np
from typing import List, Union, Optional, Dict, Any
import math
from bpodautopy.db import Engine, Connection
import pandas as pd

dbc = Connection()
dbe = Engine()
query = lambda x: pd.read_sql_query(x, dbe)


@dataclass
class Session:
    
    session_id: int = None    
    subjid: str = None
    sessiondate: date = None
    postprocess: bool = True
    
    mass: float = None

    protocol: str = None    
    settings_name: str = None
    
    num_trials: int = None
    hit: float = None
    
    trialdata: pd.DataFrame = field(default_factory=pd.DataFrame)
    settings: pd.DataFrame = field(default_factory=pd.DataFrame)
    states: pd.DataFrame = field(default_factory=pd.DataFrame)
    pokes: pd.DataFrame = field(default_factory=pd.DataFrame)
    
    
    def __post_init__(self):
        if self.session_id is not None:
            self.load_from_session_id()
        elif self.subjid is not None and self.sessiondate is not None:
            self.load_from_subjid_and_date()
        else:
            raise ValueError("You must provide either session_id or both subjid and sessiondate to initialize the Session class.")

        if self.postprocess:
            self.get_settings()
            self.get_peh()
            self.get_trialdata()
        
        
    def load_from_session_id(self):
        sess_data = query(
            f"SELECT subjid, sessiondate, protocol, num_trials, hits, mass, settings_name "
            f"FROM beh.sessview "
            f"WHERE sessid = {self.session_id} "
        )
        if sess_data.empty:        
            raise ValueError(f"No session data found for session_id: {self.session_id}")
        
        self.initialize_sessdata(sess_data)
        
        
    def load_from_subjid_and_date(self):
        """Load session data based on subjid and sessiondate."""
        
        sess_data = query(
            f"SELECT sessid "
            f"FROM beh.sessview "
            f"WHERE subjid = '{self.subjid}' AND sessiondate = '{self.sessiondate}'"
        )
        
        if sess_data.empty:
            raise ValueError(f"No session data found for subjid: {self.subjid} on {self.sessiondate}.")
    
        if len(sess_data) > 1:
            warnings.warn(
                f"Multiple session IDs found for subjid: {self.subjid} on {self.sessiondate}. "
                f"Using the first session ID: {sess_data['sessid'].iloc[0]}."
            )
        
        self.initialize_sessdata(sess_data)
        
        
        
    def initialize_sessdata(self, sess_data):
        if not sess_data.empty:
            self.subjid = sess_data['subjid'].iloc[0]
            self.sessiondate = sess_data['sessiondate'].iloc[0]
            self.protocol = sess_data['protocol'].iloc[0]
            self.num_trials = sess_data['num_trials'].iloc[0]
            self.hits = sess_data['hits'].iloc[0]
            self.mass = sess_data['mass'].iloc[0]
            self.settings_name = sess_data['settings_name'].iloc[0]


    def get_settings(self):
        
        this_df = query(
            f"SELECT settings "
            f"FROM beh.trialsview "
            f"WHERE sessid= '{self.session_id}'"
            f"ORDER by trialnum"
        )
        
        if this_df.empty:
            return pd.DataFrame()  # Return an empty DataFrame if no settings are found 

        base_settings = json.loads(this_df["settings"].iloc[0].decode('utf8'))['vals']
        this_settings = deepcopy(base_settings)

        settings_list = []
        for settings in this_df["settings"]:
            if settings is not None:
                this_change = json.loads(settings.decode('utf-8'))['vals']
                this_settings.update(this_change)  # Update base settings with changes
            settings_list.append(deepcopy(this_settings))
            
        self.settings = self.append_keys(pd.DataFrame(settings_list))
        
        
    def get_trialdata(self):
        self.trialdata = self.append_keys(pd.DataFrame(self.get_json(self.session_id, 'data')))
        
        
    def get_peh(self):
        peh_list = self.get_json(self.session_id, "parsed_events")
        peh = pd.DataFrame([peh_list[i]["StartTime"] for i in range(len(peh_list))],columns= ["start_time"])

        events_df = pd.DataFrame([peh_list[i]["Events"] for i in range(len(peh_list))])
        self.pokes = self.append_keys(pd.merge(peh, events_df, left_index=True, right_index=True))

        states_df = pd.DataFrame([peh_list[i]["States"] for i in range(len(peh_list))])
        states_df = states_df.map(lambda x: [None, None] if np.sum(pd.isna(x)) == 1 else x)
        self.states = self.append_keys(pd.merge(peh, states_df, left_index=True, right_index=True))
        
        
    def append_keys(self, df): 
        df['session_id'] = self.session_id
        df['trialnum'] = range(1, len(df) + 1)
        df['subjid'] = self.subjid
        cols = ['session_id', 'subjid', 'trialnum'] + [col for col in df.columns if col not in ['session_id', 'subjid', 'trialnum']]
        return df[cols]


    def get_all_states(self):
        if self.states.empty:
            self.get_peh()
            
        def is_not_none_pair(val):
            return val != [None, None]
        
        filtered_rows = []
        for idx, row in self.states.iterrows():
            # Filter columns in the current row where the value is not [None, None]
            filtered_row = row[row.apply(is_not_none_pair)]
            filtered_rows.append(filtered_row)

        return filtered_rows
    
    
    def get_all_pokes(self, in_only = True):
        """Finds all the pokes that happend during a trial
        
        Args:
            peh (_type_): peh as returned by get_peh
            in_only (bool, optional): Only get poke times for In events. Defaults to True.

        Returns:
            _type_: two lists of sorted poke times wrt trial start and corresponding poke times
        """
        
        if self.pokes.empty:
            self.get_peh()
        
        all_pokes = []
        poke_type = []
        for tr in range(len(self.pokes)):
            allpokes, poketype =  self.process_pokes(tr, "In")
            all_pokes.append(allpokes)
            poke_type.append(poketype)
            if in_only is False:
                allpokes, poketype =  self.process_pokes(tr, "Out")
                all_pokes.append(allpokes)
                poke_type.append(poketype)
                
        return all_pokes, poke_type
    
    
        
    def process_pokes(self, tr, key_type = "In"):
        """ Finds poke times for a given trial

        Args:
            tr (_type_): trial number
            key_type (str, optional): Find just In events. Defaults to "In".

        Returns:
            _type_: two lists of sorted poke times wrt trial start and corresponding poke times
        """
            
        keys = [item for item in self.pokes if any(sub in item for sub in ['Bot', 'Mid', 'Top'])]
        if key_type == "In":
            keys =  [item for item in keys if 'In' in item]
        elif key_type == "Out":
            keys =  [item for item in keys if 'Out' in item]

        poke_times = []
        poke_type = []
        
        for key in keys:
            value = self.pokes.loc[tr, key]
            if self.is_valid_value(value):
                poke_times.append(value)
                if isinstance(value, float) or isinstance(value, int):
                    poke_type.extend([key] * np.size(value))
                elif isinstance(value, list):
                    poke_type.extend([key] * len(value))
                    
        poke_times = self.flatten_list(poke_times)
        poke_times = [np.round(item,4) for item in poke_times]

        return poke_times, poke_type
    
    
    def process_pokes_between_states(self, states, tr):
        """given a list of states finds all the poke types that occurred in 
        in the time span of the states 

        Args:
            states (list): a list of states
            tr (int): trial number

        Returns:
            list: returns the identity of the pokes which were entered
        """

        all_pokes, poke_type = self.process_pokes(tr, key_type = "In")
        min_time = None
        max_time = None

        for st in states:
            
            if st in self.states:
                value = self.states.loc[tr, st]
                
                if np.any(value) is not None:
                    if self.is_valid_value(value):
                        if min_time is None or max_time is None:
                            min_time = np.round(value[0], 4)
                            max_time = np.round(value[1], 4)
                        else:
                            min_time = np.round(min(min_time,  value[0]),4)
                            max_time = np.round(max(max_time,  value[1]),4)
                        
        if (min_time is not None) and (max_time is not None):
            find_pokes = [i for i in range(len(all_pokes)) if all_pokes[i] > min_time and all_pokes[i] <= max_time]
            poke_type = [poke_type[i][:4] for i in find_pokes]
        else:
            find_pokes = []
            poke_type = []

        return [all_pokes[f] for f in find_pokes], poke_type
    
            
        
    def flatten_list(self, lst):
        """flattens a list of lists into a single list

        Args:
            lst (list): list of lists

        Returns:
            list: flattened list 
        """
        flattened = []
        for item in lst:
            if isinstance(item, list):
                flattened.extend(self.flatten_list(item))
            else:
                flattened.append(item)
        return flattened

    
    @staticmethod
    def get_json(session_id, data_field):
    
        this_df = query(
            f"SELECT * "
            f"FROM beh.trialsview "
            f"WHERE sessid= '{session_id}' "
            f"ORDER by trialnum"
        )
        return [json.loads(this_df[data_field][i].decode('utf8'))['vals'] for i in range(len(this_df))]


    @staticmethod
    def is_valid_value(value):
        """ Checks is the value is a `nan` as returned in peh for pokes

        Args:
            value (_type_): _description_

        Returns:
            Bool: True of all values are not `nan`, False otherwise
        """
        if isinstance(value, list):
            return all(not math.isnan(x) for x in value)
        else:
            return not math.isnan(value)
        
        


    
    
@dataclass
class Sessions:
    
    sessions: List[Session] = field(default_factory=list)
    
    # not implemented
    # criterion: Dict = field(default_factory=dict)
    
    def __init__(self, subjid: Optional[Union[str, List[str]]] = None, dates: Optional[Union[int, List, date]] = None):
        """
        Initialize multiple Session objects based on a range of dates or subject IDs.

        Args:
            subjid (str or list of str): Subject ID(s) to filter sessions.
            dates (int, list, or date): Range of dates or single date. Default is None (today's date).
        """
        date_condition = self.parse_date_range(dates)
        subjid_condition = f"AND subjid IN ('{', '.join(subjid)}')" if isinstance(subjid, list) else f"AND subjid = '{subjid}'" if subjid else ""
        
        sess_data = query(
            f"SELECT sessid "
            f"FROM beh.sessview "
            f"WHERE sessiondate {date_condition} {subjid_condition} "
            f"ORDER BY sessiondate"
        )

        if sess_data.empty:
            raise ValueError("No sessions found for the given criteria.")

        # Initialize sessions
        self.sessions = [Session(session_id=row['sessid']) for _, row in sess_data.iterrows()]


    def get_all_settings(self, concat: bool = True):
        """
        Retrieve settings for all sessions.

        Args:
            concat (bool): If True, returns concatenated DataFrame. If False, returns list of DataFrames.

        Returns:
            pd.DataFrame or List[pd.DataFrame]: Settings data for all sessions.
        """
        settings = [sess.settings for sess in self.sessions if hasattr(sess, 'settings')]
        return pd.concat(settings, ignore_index=True) if concat else settings


    def get_all_trialdata(self, concat: bool = True):
        trialdata = [sess.trialdata for sess in self.sessions if hasattr(sess, 'trialdata')]
        return pd.concat(trialdata, ignore_index=True) if concat else trialdata


    def get_all_states(self, concat: bool = True):
        states = [sess.states for sess in self.sessions if hasattr(sess, 'states')]
        return pd.concat(states, ignore_index=True) if concat else states


    def get_all_pokes(self, concat: bool = True):
        pokes = [sess.pokes for sess in self.sessions if hasattr(sess, 'pokes')]
        return pd.concat(pokes, ignore_index=True) if concat else pokes

    @staticmethod
    def parse_date_range(dates=None) -> str:
        """
        Parse a date range into SQL BETWEEN syntax.

        Args:
            dates (int, list, or date): 
                - If `int`, represents the number of days in the past (e.g., `-10` means last 10 days).
                - If `list` of integers, specifies a range in days (e.g., `[-10, -2]` means between 10 and 2 days ago).
                - If `date`, specifies a specific date.
                - If `list` of dates, specifies a range between those dates.
                - If `None`, returns a range that includes all dates.


        Returns:
            str: SQL-compatible BETWEEN clause.
        """
        if dates is None:
            date_1, date_2 = '1900-01-01', '9999-12-31'
        elif isinstance(dates, int):
            # Single integer: Range from `dates` days ago to today
            date_1 = (date.today() + timedelta(days=dates)).strftime("%Y-%m-%d")
            date_2 = date.today().strftime("%Y-%m-%d")
        elif isinstance(dates, list):
            if all(isinstance(d, int) for d in dates):
                # List of integers: Range in days
                date_1 = (date.today() + timedelta(days=dates[0])).strftime("%Y-%m-%d")
                date_2 = (date.today() + timedelta(days=dates[1])).strftime("%Y-%m-%d")
            elif all(isinstance(d, (str, date)) for d in dates):
                # List of dates: Specific range
                date_1, date_2 = sorted([pd.to_datetime(d).strftime("%Y-%m-%d") for d in dates])
            else:
                raise ValueError("Invalid date list format. Must contain either integers or dates.")
        elif isinstance(dates, (str, date)):
            # Single date
            date_1 = date_2 = pd.to_datetime(dates).strftime("%Y-%m-%d")
        else:
            raise ValueError("Invalid dates format provided.")

        if date_2 < date_1:
            raise ValueError("The second date must be later than or equal to the first date.")

        return f"BETWEEN '{date_1}' AND '{date_2}'"
