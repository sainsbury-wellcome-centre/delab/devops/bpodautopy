
import datetime
import traceback

from slack_helper import send_slack_msg, get_slack_url

def tomorrow():
    """
    Simple function to return tomorrow's date in YYYY-MM-DD format
    """
    return (datetime.date.today() + datetime.timedelta(days=1)).strftime('%Y-%m-%d')


def copy_schedule(dbc = None,from_date=None):
    '''
        copy_schedule(from_date=current_date)
        This function gets called each day by a cron job.
    '''
    try:

        if not from_date:
            from_date = tomorrow()

        dbc.call(f'met.fill_schedule("{from_date}")')
    except:
        traceback.print_exc()
        send_slack_msg("Failed copying schedule!!", traceback.format_exc(), "C04891LMETG", get_slack_url())