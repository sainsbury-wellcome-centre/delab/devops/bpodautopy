from pathlib import Path
import json
import traceback

from slack_helper import send_slack_msg,get_slack_url

ANIMAL_BOT_SLACK_CHANNEL = "C04891LMETG"

def export_octagon_RT(dbc = None):

    try: 

        subj_list = dbc.query("select unique(subjid)  \
                              from beh.sessions where sessiondate between curdate() - 14 and curdate()  and \
                              rigid like '464%' and \
                              subjid not like '%-T-%' and \
                              subjid not like '%-P-%'")

        if subj_list is None or len(subj_list) == 0:
            return

        for subjid in subj_list:
            subjid = subjid[0]
            if subjid is None:
                # no subj
                continue

            session_RT = []
            session_stim_delay = []
            # for every session get the trial data
            # TODO: this can be more efficient can only make 1 request to the DB 

            # It seems the speeded protocols change animals behavior. To prevent wait_timeouts getting faster and faster 
            # we take the RTs from before the animals experienced the speeded protocol.
            
            first_speeded = dbc.query(
                f"""
                select min(sessid) from beh.sessions 
                where protocol like "speed%" and subjid = '{subjid}'
                """
                )[0][0]

            if first_speeded:
                filter_string = f'sessid < {first_speeded} and'
            else:
                filter_string = ''

            trials_data_list = dbc.query(
                f"""
                select data from beh.sesstrialview
                where subjid = '{subjid}' and {filter_string}
                (protocol = 'stage1' OR protocol = 'visualdisc_oc') and 
                viol = 0
                order by trialtime DESC limit 300
                """)

            if trials_data_list is None or len(trials_data_list) == 0:
                send_slack_msg(f"Could not load trial data for {subjid}",f"",ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())
                continue

            fault_occurred = False

            for trial_data in trials_data_list:
                trial_data_dict = json.loads(trial_data[0])
                # print(trial_data_dict)

                if 'RT' in trial_data_dict.keys():
                    session_RT.append(trial_data_dict['RT'])
                else:
                    send_slack_msg(f"Could not find RT data in one of the trials!: {subjid}",f"",ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())
                    fault_occurred = True
                    break
                    
                if 'stimulus_delay' in trial_data_dict.keys():
                    session_stim_delay.append(trial_data_dict['stimulus_delay'])
                else:
                    send_slack_msg(f"Could not find stimulus_data data in one of the trials!: {subjid}",f"",ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())
                    fault_occurred = True
                    break
            
            if fault_occurred:
                # go to next subj
                continue
            
            if len(session_RT) != len(session_stim_delay):
                send_slack_msg(f"Error the data for trial not read properly : {subjid}", f"",ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())
                continue



            difference_rt_stim = [round(session_RT[i] - session_stim_delay[i],3) for i in range(len(session_RT))]
            
            rt_dict_str = json.dumps({'RT':difference_rt_stim})

            # save to the filepath 
            dir = f"/www-delab/conf/rt/{subjid}/"
            filepath = dir +'/RT.json'
            # ensure directory present
            Path(dir).mkdir(parents=True, exist_ok=True)

            with open(filepath,'w+') as f:
                f.write(rt_dict_str)

    except Exception as e:
        traceback.print_exc()
        response = send_slack_msg(f":bangbang: Error while saving the RT for octagon! Error: {str(e)}","",ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())



