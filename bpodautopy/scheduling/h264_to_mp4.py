import subprocess
import re
import glob
import os
import time


# get current directory
current_dir = os.getcwd()
bash_script = os.path.join(current_dir, 'scheduling/h264_to_mp4.sh')


def run_conversion_process(dir, root, log_dir):
    print(f'Converting {dir}')
    subprocess.call([bash_script, f'{root}/{dir}', log_dir])
    print(f'Finished converting {dir}')



def test_modified(folder_name):
    '''checking to see if the directory was modified in last 4 days'''
    last_changed = max(os.path.getmtime(root) for root,_,_ in os.walk(folder_name))
    
    delta = time.time() - last_changed
    delta = delta / (60*60*24)
    if delta < 1:
        return True
    return False
def test_valid_subj_dir(folder_name):
    # check if the folder ends with regex AAA-A-0001
    regex1 = re.compile('[A-Z]{3}-[A-Z]-\d{4}')
    regex2 = re.compile('[A-Z]{3}-\d{7}')
    if regex1.match(folder_name[-10:]) or regex2.match(folder_name[-11:]):
        return True
    return False



if __name__ == '__main__':
    print('Converting h264 to mp4')

    convert_only_latest = False

    camera_data_dirs_list = [
                            '/ceph/ogma/headfixed/camera-data', 
                            '/ceph/ogma/RAW/370001/Camera',
                            # '/ceph/ogma/RAW/370002/Camera',
                            '/ceph/ogma/RAW/368001/Camera'
                            ]
    
    log_dirs_list = [
                    '/ceph/ogma/headfixed/camera-data',
                    '/ceph/ogma/RAW/370001/Camera',
                    # '/ceph/ogma/RAW/370002/Camera',
                    '/ceph/ogma/RAW/368001/Camera'
                ]
    
    
    

    for camera_data_dir, log_dir in zip(camera_data_dirs_list, log_dirs_list):

        try: 
            sub_dir_list = next(os.walk(camera_data_dir))[1]
            sub_dir_list = [os.path.join(camera_data_dir, sub_dir) for sub_dir in sub_dir_list]
        except Exception:
            continue


        dir_to_convert = [dir for dir in sub_dir_list if test_valid_subj_dir(dir)]
        if convert_only_latest:
            dir_to_convert = [dir for dir in sub_dir_list if test_modified(dir)]
        print(dir_to_convert)
        

        for root in dir_to_convert:
            # get all directories in the directory
            directory_list = next(os.walk(root))[1]
            if convert_only_latest:
                directory_list = [dir for dir in directory_list if test_modified(root + '/'+ dir)]
            print(directory_list)
            for dir in directory_list:
                run_conversion_process(dir, root,log_dir)


            
