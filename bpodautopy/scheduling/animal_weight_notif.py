import pandas as pd
import traceback
import datetime

from slack_helper import send_slack_msg,get_slack_url
import weight_alarm as wa
from db import Engine, Connection

ANIMAL_BOT_SLACK_CHANNEL = "C04891LMETG"

MASS_UPLOAD_DIR = '/mnt/ogma/delab/mass/'
WATER_RESTRICTION_WEIGHT_CHECK_CSV = MASS_UPLOAD_DIR + 'water_restriction_daily_weight_check.csv'

BW_WARNING_THRESHOLD_MOUSE = 0.80
BW_WARNING_THRESHOLD_RAT = 0.90
RAT_REWARD_WARNING_THRESHOLD = 5000

global weight_alarm_sent_flag,checking_realtime_free_animals_weight
weight_alarm_sent_flag = False
checking_realtime_free_animals_weight = True

def reset_weight_notify_status():
    global weight_alarm_sent_flag,checking_realtime_free_animals_weight
    weight_alarm_sent_flag = False


def get_slack_userid_from_animal_list(subjlist, dbc):
    if subjlist:
        subj_str = ''
        for subj in subjlist:
            subj_str += ("'"+subj+"',")
        subj_str = f"({subj_str[:-1]})"
        sqlstr = f'''
            select slackuser from met.experimenters 
            where experid in 
            (select DISTINCT experid 
            from met.animals where subjid in {subj_str})
        '''
        out = dbc.query(sqlstr)
        slack_user_list = [i[0] for i in out]
        slack_user_str = ''
        for slack_user in slack_user_list:
            slack_user_str += ("<@"+slack_user+"> ")
    else:
        slack_user_str = ''
    return slack_user_str

def push_nofication_reward_amount(dbe = None):
    #currently this only have rats
    sqlstr = f'''
        select subjid,owner,total_reward 
        from met.today_mass_summary 
        where total_reward < {RAT_REWARD_WARNING_THRESHOLD:.0f} and baseline>70
    '''
    df = pd.read_sql_query(sqlstr, dbe)
    if len(df)>0:
        message = (df.to_markdown(index=False))
        message = "```\n" + message + "\n```"
        title_text = ":low_battery: Animals did not get enough water in training today!"
        response = send_slack_msg(title_text,message,ANIMAL_BOT_SLACK_CHANNEL, get_slack_url())

def push_notification_weight(dbc = None):
    response = ""
    try:
        message = wa.orgnize_info_in_markdown(dbc)
        title_text = "Animal Weight Alarm :mouse2: "
        response = send_slack_msg(title_text,message,ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())
    except:
        traceback.print_exc()
        response = send_slack_msg(":bangbang: You have Error Proceesing Weight Warning, Please Check The Error Message","",ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())
    return response


def weekly_weight_csv_for_pyrat_writer(dbe):
    """
    This function will write the the csv to upload daily mass to pyrat
    accessible at http://lab.deneuro.org/mass/weekly_mass_to_pyrat_{M/R}.csv

    Seperate csv for rats and mice
    """
    species = ['mouse' , 'rat']

    for s in species:
        sqlstr = f'''
            select
                pyratid as Identifier,
                cast(mass_date AS DATE) as Date,
                mass as Weight
            from
                met.massview
            where
                mass_date >= NOW() - INTERVAL 7 DAY
                and species = '{s}'
            group by 
                Identifier, Date    
            order by 
                Identifier, Date desc;
        '''
        try:
            df = pd.read_sql_query(sqlstr, dbe)
            upload_location = MASS_UPLOAD_DIR + f"weekly_mass_to_pyrat_{s}_{datetime.datetime.today().strftime('%Y-%m-%d')}.csv"
             
            df.to_csv(upload_location,index=False)

        except:
            traceback.print_exc()

def push_notif_weekly_pyrat_weights(dbe = None):

    assert dbe is not None, "No database engine provided"

    try:
        weekly_weight_csv_for_pyrat_writer(dbe)

        # Tagging Chris and Cong for the notification
        message = f"""
        Upload for <http://lab.deneuro.org/mass/weekly_mass_to_pyrat_mouse_{datetime.datetime.today().strftime('%Y-%m-%d')}.csv|Mice pyRat Weights> 
        and <http://lab.deneuro.org/mass/weekly_mass_to_pyrat_rat_{datetime.datetime.today().strftime('%Y-%m-%d')}.csv|Rat pyRat Weights>

        has been generated and is ready for download.

        <@U05RRECRY0N> <@U04DEDDV544>
        """
        title_text = "PyRat Weekly Weight Uploads :mouse2: :rat:"
        response = send_slack_msg(title_text,message,ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())
    except:
        traceback.print_exc()
        response = send_slack_msg(None,":bangbang: You have Error Proceesing Weight Warning, Please Check The Error Message",ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())




def daily_weight_check_water_restriction_animals(dbc):
    """
    This function will write the the csv to monitor daily weight
    accessible at http://lab.deneuro.org/mass/water_restriction_daily_weight_check.csv
    """
    try:
        mass_date_since = '"' + datetime.datetime.today().replace(day=1).strftime('%Y-%m-%d') + '"' #first day of this month
        dbc.use('met')
        out_controlled_animals = dbc.query('select subjid from animals where water = "controlled" and species != "tester"')
        out_controlled_animals = [i[0] for i in out_controlled_animals]
        sqlstr = '''
            with baseline_mass as (select
            animals.subjid as subjid,
            animals.pyratid as pyratid,
            animals.pyratcageid as pyratcageid,
            mass.mass as baseline
            from animals left join water_restriction_baseline on animals.subjid = water_restriction_baseline.subjid 
            left join mass on water_restriction_baseline.massid = mass.massid 
            where animals.water = "controlled" and animals.species != "tester" and animals.status != 'dead' 
            order by subjid),
            daily_mass as 
            (select subjid,mass,mdate as mass_date 
            from mass 
            where mdate > {} 
            and massid in (select max(massid) id 
            from mass 
            group by cast(mdate AS DATE),subjid)) 
            select baseline_mass.subjid as subjid,
            baseline_mass.pyratid as pyratid,
            baseline_mass.pyratcageid as pyratcageid,
            baseline_mass.baseline as baseline,
            mass_date,mass,mass/baseline as weight_percentage 
            from baseline_mass left join daily_mass 
            on baseline_mass.subjid = daily_mass.subjid
        '''.format(mass_date_since)
        out = dbc.query(sqlstr)

        if len(out) >= len(out_controlled_animals):#must have data to print
            subjid_uq = out_controlled_animals
            subj_list = [i[0] for i in out]
            date_uq = list(set([i[4].strftime("%Y-%m-%d") for i in out]))
            date_uq = sorted(date_uq)
            header = ['subjid','pyratid','pyrat cage','baseline mass'] + date_uq

            df = pd.DataFrame(columns=header)
            ii = 0
            for this_subj in subjid_uq:
                indices = [i for i, x in enumerate(subj_list) if x == this_subj]
                if indices:#cannot be empty
                    this_subj_row = [this_subj,out[indices[0]][1],out[indices[0]][2],f'{out[indices[0]][3]:.1f}'+'g'] + [None] * len(date_uq)
                    for this_ind in indices:
                        this_record = out[this_ind]
                        this_record_date = this_record[4].strftime("%Y-%m-%d")
                        this_subj_row[date_uq.index(this_record_date)+4] = f'{this_record[5]:.1f}' + 'g/' + f'{this_record[6]*100:.1f}' + '%'
                        #print(this_subj_row)
                    df.loc[ii] = this_subj_row
                    ii += 1
                
            df.to_csv(WATER_RESTRICTION_WEIGHT_CHECK_CSV,index=False)
    except:
        traceback.print_exc()
        response = send_slack_msg(":bangbang: You have Error For Water Restriction Table, Please Check The Error Message","",ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())


def push_notification_water_restricted_animals_done(dbe = None, dbc = None):
    try:
        sql = '''
            select b.subjid,
            b.pyratid,
            b.pyratcageid,
            a.mass,
            b.owner 
            from met.mass a right join met.animals b 
            on (a.subjid = b.subjid and CAST(a.mdate AS DATE) =  DATE(NOW())) 
            where b.water = "controlled" 
            and b.status != "dead" 
            and b.species != "tester"
        '''
        global weight_alarm_sent_flag
        if weight_alarm_sent_flag:
            # weight alarm have been sent today, pass
            pass
        else:
            # weight alarm not sent yet
            # notify the completion of weighting if it's already done
            df = pd.read_sql_query(sql, dbe)
            df2 = df[df["mass"].isnull()]
            if len(df2)<1: #all animals have been weighted
                message = "All Water Restricted Animals:mouse2: Have Been Weighed Today! :tada:"
                response = send_slack_msg(None,message,ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())
                # push_notification_weight_percentage_problem_animals(dbe,dbc)
                # push_nofication_reward_amount(dbe)
                push_notification_weight(dbc)
                daily_weight_check_water_restriction_animals(dbc)
                # notification message sent
                weight_alarm_sent_flag = True
    except:
        traceback.print_exc()
        response = send_slack_msg(":bangbang: You have Error push_notification_water_restricted_animals_done, Please Check The Error Message","",ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())

def check_realtime_free_animals_weight_upload(dbe = None, dbc = None):
    try:
        global checking_realtime_free_animals_weight
        if checking_realtime_free_animals_weight:
            sql = '''
                select b.subjid,
                b.pyratid,
                b.pyratcageid,
                a.mass,
                b.owner 
                from met.mass a right join met.animals b 
                on (a.subjid = b.subjid 
                and CAST(a.mdate AS DATE) > DATE(NOW() - INTERVAL 7 DAY)) 
                where b.water = "free" 
                and b.status = "free" 
                and b.species != "tester"
            '''
            df = pd.read_sql_query(sql, dbe)
            df2 = df[df["mass"].isnull()]
            if len(df2)<1:#all free animals have been done today
                checking_realtime_free_animals_weight = False
                
                message = "All Free Animals :droplet: Have Been Weighed TODAY! :tada: "
                response = send_slack_msg(None,message,ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())
                daily_weight_check_water_restriction_animals(dbc)
    except:
        traceback.print_exc()
        response = send_slack_msg(":bangbang: You have Error in check_realtime_free_animals_weight_upload, Please Check The Error Message","",ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())

def push_notification_normal_animals(dbe = None, dbc = None):
    try:
        sql = '''
            select b.subjid,
            b.pyratid,
            b.pyratcageid,
            a.mass,
            b.owner 
            from met.mass a right join met.animals b 
            on (a.subjid = b.subjid 
            and CAST(a.mdate AS DATE) > DATE(NOW() - INTERVAL 7 DAY)) 
            where b.water = "free" 
            and b.status = "free" 
            and b.species != "tester"
        '''
        global checking_realtime_free_animals_weight
        df = pd.read_sql_query(sql, dbe)
        df2 = df[df["mass"].isnull()]
        if len(df2)>0:
            dflist = df2.values.tolist()
            subj_list = [i[0] for i in dflist]
            slack_mentions = get_slack_userid_from_animal_list(subj_list,dbc)
            message = (df2.to_markdown(index=False))
            message = "```\n" + message + "\n```"
            title_text = f":warning:Free Animals :droplet: Not Been Weighed Over 7 Days!{slack_mentions}"
            checking_realtime_free_animals_weight = True
        else:
            message = " "
            title_text = "All Free Animals :droplet: Have Been Weighed Recently! :tada: "
            checking_realtime_free_animals_weight = False

        response = send_slack_msg(title_text,message,ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())
        # today_weight_csv_for_pyrat_writer(dbe)
        daily_weight_check_water_restriction_animals(dbc)
        return response
    except:
        traceback.print_exc()
        response = send_slack_msg(":bangbang: You have Error in push_notification_normal_animals, Please Check The Error Message","",ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())

def push_notification_if_missing_water_restricted_animals_today(dbe = None, dbc = None):
    try:
        sql = '''
            select b.subjid,
            b.pyratid,
            b.pyratcageid,
            a.mass,
            b.owner 
            from met.mass a right join met.animals b 
            on (a.subjid = b.subjid and CAST(a.mdate AS DATE) = CAST(curdate() AS DATE)) 
            where b.water = "controlled" 
            and b.status != "dead" 
            and b.species != "tester"
        '''
        df = pd.read_sql_query(sql, dbe)
        df2 = df[df["mass"].isnull()]
        if len(df2)>0: #have missing animals
            dflist = df2.values.tolist()
            subj_list = [i[0] for i in dflist]
            slack_mentions = get_slack_userid_from_animal_list(subj_list, dbc)
            message = (df2.to_markdown(index=False))
            message = "```\n" + message + "\n```"
            title_text = f":warning:Water Restricted Animals:mouse2: Not Been Weighed Today! {slack_mentions}"
            response = send_slack_msg(title_text,message,ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())
            daily_weight_check_water_restriction_animals(dbc)
    except:
        traceback.print_exc()
        response = send_slack_msg(":bangbang: You have Error in push_notification_if_missing_water_restricted_animals_today, Please Check The Error Message","",ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())


def push_notification_weight_percentage_problem_animals(dbe, dbc):
    sql = f'''
        select 
        a.subjid as subjid,
        a.pyratid as pyratid,
        a.pyratcageid as pyratcageid,
        a.owner as owner,
        a.mass as mass,
        a.weight_percentage as weight_percentage 
        from met.today_mass_summary a join met.animal_info b on (a.subjid=b.subjid)
        where (b.species = 'rat' and a.weight_percentage<{BW_WARNING_THRESHOLD_RAT:.2f}) or (b.species='mouse' and a.weight_percentage<{BW_WARNING_THRESHOLD_MOUSE:.2f})
    '''
    df = pd.read_sql_query(sql, dbe)
    if len(df)>0:
        dflist = df.values.tolist()
        subj_list = [i[0] for i in dflist]
        slack_mentions = get_slack_userid_from_animal_list(subj_list,dbc)
        message = (df.to_markdown(index=False))
        message = "```\n" + message + "\n```"
        title_text = f":warning: LOW BW animals! please check/watering! (Threshold: Rat:{round(BW_WARNING_THRESHOLD_RAT*100)}% Mouse:{round(BW_WARNING_THRESHOLD_MOUSE*100)}% ){slack_mentions}"
        response = send_slack_msg(title_text,message,ANIMAL_BOT_SLACK_CHANNEL,get_slack_url())

if __name__ == '__main__':

    dbe = Engine()

    push_notif_weekly_pyrat_weights(dbe)