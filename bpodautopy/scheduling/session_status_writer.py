import traceback
import pandas as pd

from slack_helper import send_slack_msg, get_slack_url

# get home directory
import os
HOME = os.path.expanduser("~")

write_to_ceph = False

SESSION_STATUS_CSV = HOME + '/.deneuro/conf/rig_status.csv'


if write_to_ceph:
    SESSION_STATUS_CSV = '/mnt/ogma/delab/conf/rig_status.csv'

def session_status_writer(dbe = None):
    """
    This function will write the session status from the database to a csv that is web
    accessible at http://lab.deneuro.org/conf/rig_status.csv

        dbe: Database Engine
    """

    sql ="""
        select
            cs.rigid,
            rs.status as rig_status,
            rv.isbroken as is_rig_broken,
            rv.istesting as is_testing,
            rs.gitlaburl,
            rs.gitlabuser,
            cs.hostip,
            cs.sessid,
            cs.sessiondate,
            cs.subjid,
            a.species,
            a.rfid,
            cs.start_time as starttime,
            cs.protocol,
            cs.startstage,
            cs.stattime,
            cs.sess_status as session_status,
            count(trialid) as lastTrial,
            ifnull(round(sum(reward)), 0) as reward,
            ifnull(round(avg(viol>0)* 100), 0) as violation,
            ifnull(round(avg(hit)* 100), 0) hit
        from
            beh.latest_sessview cs
        left join (
            met.rig_view rv,
            met.current_rig_status rs) ON
            (rv.rigid = cs.rigid
                and rs.rigid = cs.rigid)
        left join met.animal_info a
        on
            (cs.subjid = a.subjid)
        left join beh.trials t
                using (sessid)
        group by
            cs.sessid
        order by
            rigid;
        """
    try:
        df = pd.read_sql_query(sql, dbe)
        df.to_csv(SESSION_STATUS_CSV)
    except:
        traceback.print_exc()
        print("Failed writing session status to csv!!")
        send_slack_msg("Failed writing session status to csv!!", "", "C04891LMETG", get_slack_url())