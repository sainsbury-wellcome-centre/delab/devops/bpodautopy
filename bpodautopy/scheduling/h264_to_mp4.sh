#!/bin/bash

# Check if an argument was provided.
if [ $# -eq 0 ]; then
    echo "No directory provided. Usage: $0 /path/to/directory" 
    exit 1
fi

BASE_DIR="$1"
# echo "BASE_DIR: $BASE_DIR" 
TIME=`date +%Y-%m-%d_%H-%M-%S`


cd "$BASE_DIR"

# get logging path from the 2nd args
if [ $# -eq 2 ]; then
    LOG_FILE="$2/.mp4mux.log"
else
    LOG_FILE="$BASE_DIR/.mp4mux.log"
fi

echo "LOG_FILE: $LOG_FILE"
touch $LOG_FILE



# Find all h264 files in the directory tree.
find "$BASE_DIR" -type f -name "*cam*.json" | while read -r FILE; do
    echo '' >> $LOG_FILE
    IN_FILE=`jq ".acquisition_files.video" "$FILE"`
    IN_FILE=`echo "$IN_FILE" | tr -d '"'`
    # get the filename from the whole path 
    IN_FILE=`basename "$IN_FILE"`
    OUT_FILE="${IN_FILE%.h264}.mp4"
    fps=`jq ".acquisition_settings.framerate" "$FILE"`
    # print out and in file and fps 
    echo "PROCESSING IN_FILE: $IN_FILE OUT_FILE: $OUT_FILE FPS: $fps" >> $LOG_FILE
    # time stamp
    echo "TIME: `date +%Y-%m-%d_%H-%M-%S`" >> $LOG_FILE
    if [ -f "$OUT_FILE" ]; then
        echo "---WARNING:SKIP File $OUT_FILE already exists, skipping" >> $LOG_FILE
        continue
    fi

    # Get the original file size.
    ORIG_SIZE=$(stat -c%s "$IN_FILE")

    # Mux the h264 file to mp4 using mp4box
    MP4Box -fps "$fps" -add "$IN_FILE" "$OUT_FILE" -new

    # Check if the mp4 file was created successfully.
    if [ -f "$OUT_FILE" ]; then
        # Get the new file size.
        NEW_SIZE=$(stat -c%s "$OUT_FILE")
        echo "---File Size original $ORIG_SIZE new $NEW_SIZE " >> $LOG_FILE

        # Check if the file sizes are roughly the same.
        if (( NEW_SIZE >= ORIG_SIZE * 95 / 100 && NEW_SIZE <= ORIG_SIZE * 105 / 100 )); then
            echo "---SUCCESS" >> $LOG_FILE
            echo "---Successfully muxed $IN_FILE to $OUT_FILE" >> $LOG_FILE

            # If the mp4 file was created successfully, delete the original h264 file.
            # rm "$IN_FILE"
            # echo "---Deleted original file $IN_FILE" >> $LOG_FILE

        elif (( NEW_SIZE >= ORIG_SIZE * 90 / 100 && NEW_SIZE <= ORIG_SIZE)); then
            echo "---WARNING:SIZE  File size differs too much from original file"  >> $LOG_FILE
            echo "---OUT_SIZE: $NEW_SIZE ; IN_SIZE: $ORIG_SIZE "  >> $LOG_FILE
            echo "---OUT: $OUT_FILE"  >> $LOG_FILE
            echo "---IN : $IN_FILE"  >> $LOG_FILE
            echo "---!! Please CHECK output file: $OUT_FILE" >> $LOG_FILE
        else
            echo "---ERROR:SIZE  File size differs too much from original file"  >> $LOG_FILE
            echo "---OUT_SIZE: $NEW_SIZE ; IN_SIZE: $ORIG_SIZE "  >> $LOG_FILE
            echo "---OUT: $OUT_FILE"  >> $LOG_FILE
            echo "---IN : $IN_FILE"  >> $LOG_FILE
            rm "$OUT_FILE"
            echo "---Removing output file: $OUT_FILE" >> $LOG_FILE
        fi
    else
        echo "---Failed to mux $IN_FILE " >> $LOG_FILE
    fi
done


