import pandas as pd
import datetime
import traceback

from slack_helper import send_slack_msg,get_slack_url


def query_db_schedule_compare(room_id, dbe = None)->list:
    '''
    This functon will check if the schedule is same as that of yesterday 
    params:
        dbe: database engine object
    return:
        list of cages with different schedule


    '''

    sqlstr = f'''
        select
        a.rigid,
        b.slotdate,
        b.slottime,
        c.subjid,
        c.cageid,
        c.owner,
        d.protocol,
        d.settingsname
    from
        met.rig_view as a
    left join met.schedule as b on
        a.rigid = b.rigid
        and b.slotdate = '__date__'
    left join met.animals as c
            using (subjid)
    left join met.current_settings 
            as d
            using (subjid)
    where
        a.autorun = 1
        and round(a.rigid/1000) = {room_id} 
    ORDER BY b.slottime ASC 
    '''

    # get yesterday's schedule
    yesterday = (datetime.date.today() - datetime.timedelta(days=1)).strftime('%Y-%m-%d')
    # get a df for yesterday's schedule
    yesterday_schedule_df = pd.read_sql_query(sqlstr.replace('__date__',yesterday), dbe).drop(columns=['slotdate'])

    # get today's schedule
    today = datetime.date.today().strftime('%Y-%m-%d')
    # get a df for today's schedule
    today_schedule_df = pd.read_sql_query(sqlstr.replace('__date__',today), dbe).drop(columns=['slotdate'])

    # cncat the two df and drop duplicates
    diff_df = pd.concat([yesterday_schedule_df,today_schedule_df]).drop_duplicates(keep=False, )
    # if the diff_df is empty then the schedule is same as that of yesterday
    if diff_df.empty:
        return []
    else:
        # return cageid in a list from diff_df
        return list(set(diff_df['rigid'].tolist()))

def check_schedule_rooms(roomid: str = None, room_channel_id = None, dbe = None):

    '''
    This function will check if the schedule is same as that of yesterday.
    If it is not the same post a message on slack channel 
    params:
        dbe: database engine object
    return:
        list of rooms with different schedule
    '''
    # see if schedule changed 
    # get a list of rigs that changed 
    try:
        assert roomid is not None, "roomid is None"
        assert room_channel_id is not None, "room_channel_id is None"

        cages_changed = query_db_schedule_compare(roomid, dbe)
        if cages_changed and len(cages_changed) > 0:
            # post a message on slack channel
            cages_changed_str = [str(cage) for cage in cages_changed] 
            cages_changed_str= ', '.join(cages_changed_str)
            title_text = "Schedule Change Warning"
            slack_message = f':warning: Schedule changed for cages {cages_changed_str} in room {roomid}'
            send_slack_msg(title_text,slack_message,room_channel_id,get_slack_url())
    except:
        traceback.print_exc()
        # send_slack_msg(":bangbang: You have Error Proceesing Schedule Change Warning","",room_channel_id,get_slack_url())


    
def run_compare(dbe = None):
    rooms = {
        '172': 'C03G7HG60KE',
        '373': 'C03G7HG60KE',
        #'373': 'C040RQWEYDN'
    }

    for roomid, room_channel_id in rooms.items():
        check_schedule_rooms(roomid, room_channel_id, dbe)
