import traceback
import pandas as pd

from slack_helper import send_slack_msg, get_slack_url

# get home directory
import os
HOME = os.path.expanduser("~")

write_to_ceph = False

CAGE_WATER_CSV = HOME + '/.deneuro/conf/current_cage_water.csv'
CURRENT_ANIMAL_CAGE = HOME + '/.deneuro/conf/current_animal_cage.csv'

if write_to_ceph:
    CAGE_WATER_CSV = '/mnt/ogma/delab/conf/current_cage_water.csv'
    CURRENT_ANIMAL_CAGE = '/mnt/ogma/delab/conf/current_animal_cage.csv'


def current_cage_water_writer(dbe = None):
    """
    This function will write the cage water status from the database to a csv that is web
    accessible at http://lab.deneuro.org/conf/current_cage_water.csv
    """

    sql = """
        select * from met.current_cage_water
        """
    try:
        df = pd.read_sql_query(sql, dbe)
        df.to_csv(CAGE_WATER_CSV)
    except:
        traceback.print_exc()
        print("Failed writing current cage water status to csv!!")
        send_slack_msg("Failed writing current cage water status to csv!!", traceback.format_exc(), "C04891LMETG", get_slack_url())

def current_animal_cage_writer(dbe = None):
    """
    This function will write the animal cage status from the database to a csv that is web
    accessible at http://lab.deneuro.org/conf/current_animal_cage.csv
    """

    sql = """
            select * from met.current_animal_cage where cageid is not NULL
        """
    try:
        df = pd.read_sql_query(sql, dbe)
        df.to_csv(CURRENT_ANIMAL_CAGE)
    except:
        traceback.print_exc()
        print("Failed writing current animal cage status to csv!!")
        send_slack_msg("Failed writing current animal cage status to csv!!", traceback.format_exc(), "C04891LMETG", get_slack_url())