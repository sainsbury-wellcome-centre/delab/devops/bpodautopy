"""
This file has helper functions to monitor the 
status of the duplicate db.
Original DB: db.deneuro.org
Duplicate DB: rodb.deneuro.org
"""

from db import Connection
from slack_helper import send_slack_msg,get_slack_url
from gitlab_helper import create_new_issue
import traceback
import configparser as cp

TECHNICAL_CHANNEL_ID = 'C03NM96HW9Z' #devops

def check_last_session_id(dbc = None, dbc_copy = None):
    '''
    Check the last session id in both databases
    '''

    # Get last session id from original db
    query = 'SELECT MAX(sessid) FROM beh.sessions'

    max_sess_id_dbc = list(dbc.get(query))[0][0]

    # Get last session id from duplicate db
    max_sess_id_dbc_copy = list(dbc_copy.get(query))[0][0]

    # Check if they are the same
    if max_sess_id_dbc == max_sess_id_dbc_copy:
        return True
    
    return False
    
def count_sessions_today(dbc = None, dbc_copy = None):
    '''
    Count the number of sessions today in both databases
    '''

    # Get last session id from original db
    query = 'SELECT COUNT(*) FROM sessions WHERE DATE(start_time) = CURDATE()'

    count_dbc = dbc.get(query)[0]

    # Get last session id from duplicate db
    count_dbc_copy = dbc_copy.get(query)[0]

    # Check if they are the same
    if count_dbc == count_dbc_copy:
        return True
    else:
        return False


def monitor_db():

    try: 

        dbc = Connection('client')

        dbc_copy = Connection('client_backup')

        # Check if last session id is the same
        if check_last_session_id(dbc, dbc_copy):
            slack_message = "DB Replication Successful"
            send_slack_msg(None,slack_message,TECHNICAL_CHANNEL_ID,get_slack_url())
        else:
            
            # create the gitlab issue
            issue_title = "DB Replication Problem"
            issue_description = """
                The DB replica is not up to date.  Check the status of replication.
                ```
                ssh rodb.deneuro.org
                sudo -s
                tmux at
                ```
                There should be a running mysql connection. if not you can connect with `mysql -u root -p`.
                In the MySQL console run `show slave status\G`

            """
            project_id = 30093470 # Hardware
            assignees = "12400350,3729571" # These IDs belong to Mehul and Jeff
            issue_link,issue_id = create_new_issue(project_id = project_id,
                                                    title = issue_title,
                                                    description= issue_description, 
                                                    assignees = assignees)
            
            # if not send a message to slack 
            title_text = ":exclamation: DB Replication Failed :exclamation: "
            slack_message = f'[Issue:#{issue_id}]({issue_link}) Check replication @Mehul @Jeff '
            send_slack_msg(title_text,slack_message,TECHNICAL_CHANNEL_ID,get_slack_url())

    except:
        traceback.print_exc()

if __name__ == '__main__':
    monitor_db()