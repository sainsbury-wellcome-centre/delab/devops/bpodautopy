from datetime import datetime
import pandas as pd
from db import Connection

CSV_FILENAME = 'weight_storage.csv'
mass_date_since = '"' + datetime.today().replace(day=1).strftime('%Y-%m-%d') + '"' #first day of this month

dbc = Connection('manage')
dbc.use('met')
out_controlled_animals = dbc.query('select subjid from animals where water = "controlled" and species != "tester"')
out_controlled_animals = [i[0] for i in out_controlled_animals]
subj_str = ""
for subj in out_controlled_animals:
    subj_str += ("'"+subj+"',")
subj_str = "(" + subj_str[:-1] + ")"
print(subj_str)
subj_str = "','".join(out_controlled_animals)
subj_str = f"('{subj_str}')"
print(subj_str)
#subj_str = "('JLI-M-0004','JLI-M-0005','JLI-M-0006','JLI-M-0010','CFB-M-0001','CFB-M-0002')"
sqlstr = '''
    with baseline_mass as (select subjid,max(mass) as mean_bs_mass 
    from massview where subjid in {} 
    and water = "free" group by subjid),
    daily_mass as 
    (select subjid,pyratid,pyratcageid,mass,mass_date 
    from massview 
    where subjid in {} 
    and water = "controlled" 
    and mass_date > {} 
    and id in (select max(id) id 
    from massview 
    group by cast(mass_date AS DATE),subjid)) 
    select baseline_mass.subjid as subjid,
    pyratid,
    pyratcageid,
    baseline_mass.mean_bs_mass as baseline,
    mass_date,mass,mass/mean_bs_mass as weight_percentage 
    from baseline_mass,daily_mass 
    where baseline_mass.subjid = daily_mass.subjid
    '''.format(subj_str,subj_str,mass_date_since)
out = dbc.query(sqlstr)

subjid_uq = out_controlled_animals
subj_list = [i[0] for i in out]
date_uq = list(set([i[4].strftime("%Y-%m-%d") for i in out]))
date_uq = sorted(date_uq)
header = ['subjid','pyratid','pyrat cage','baseline mass'] + date_uq
print(header)
#for each_date in date_uq:
df = pd.DataFrame(columns=header)
ii = 0
for this_subj in subjid_uq:
    indices = [i for i, x in enumerate(subj_list) if x == this_subj]
    this_subj_row = [this_subj,out[indices[0]][1],out[indices[0]][2],f'{out[indices[0]][3]:.1f}'+'g'] + [None] * len(date_uq)
    for this_ind in indices:
        this_record = out[this_ind]
        this_record_date = this_record[4].strftime("%Y-%m-%d")
        this_subj_row[date_uq.index(this_record_date)+4] = f'{this_record[5]:.1f}' + 'g/' + f'{this_record[6]*100:.1f}' + '%'
        #print(this_subj_row)
    df.loc[ii] = this_subj_row
    ii += 1
df.to_csv(CSV_FILENAME,index=False)
print(df)