'''
A python module for managing animals in the Erlich Lab.
The module provides basic functions for changing the status of animals.
Depends on helpers.net and helpers.DBUtilsClass and a properly configured ~/.dbconf

JCE 2017
'''
from __future__ import print_function
import warnings
import sys
import time
import json
import requests
import traceback as tb
import pandas as pd
from datetime import datetime
from datetime import timedelta
from db import Engine, Connection
import net
import pdb
import re


BPOD_BOT_CHANNEL_ID = "C03HGD6A43X"
global dbc
dbc = Connection('manage')
DBE = Engine('manage')

dbc.use('met')
#dbc.use('mettest') # for testing

def reconnect():
	global dbc
	dbc = Connection('manage')
	dbc.use('met')

def get_all_alive_animal_list():
	dbo = dbc.query("select DISTINCT subjid from met.animals where status != 'dead' and status != 'ordered' and status != 'arrived' and species<>'tester'")
	subjid_list_alive = [i[0] for i in dbo]
	return subjid_list_alive

def get_all_running_animal_list():
	dbo = dbc.query("select DISTINCT subjid from met.animals where status = 'running'")
	subjid_list_alive = [i[0] for i in dbo]
	return subjid_list_alive

def export_mass_for_pyrat(subjid=None,start_date=None,end_date=None,owner=None):
	'''
	subj, list of subjid, or just one subjid. skip this input if uploading for all animals
	start_date: start date of mass to upload,skip if you want to upload all weight data
	end_date: end date of mass to upload, skip if you want to upload till today
	owner: filter by owner (if you only want to upload your own animals), skip if you want to upload all animal

	example:
	ma.export_mass_for_pyrat(subjid='JLI-M-0001,JLI-M-0002,JLI-M-0003',start_date='2022-08-01',end_date='2022-09-01',owner='Jingjie Li')

	if you only want to upload for today, you can do
	ma.export_mass_for_pyrat(start_date='2022-09-02') #this date is the date of today
	'''

	sqlstr_part1 = 'select pyratid as Identifier,cast(mass_date AS DATE) as Date,mass as Weight from met.massview where '
	if subjid is None:
		sqlstr_part2 = ''
	else:
		subjid_list = subjid.split(",")
		print(subjid_list)
		subj_str = ""
		for subj in subjid_list:
			subj_str += ("'"+subj+"',")
		subj_str = "(" + subj_str[:-1] + ")"
		sqlstr_part2 = 'subjid in {} '.format(subj_str)

	if start_date is None:
		sqlstr_part3 = ''
	else:
		sqlstr_part3 = "mass_date > '{}' ".format(start_date)

	if end_date is None:
		sqlstr_part4 = ''
	else:
		sqlstr_part4 = "mass_date < '{}' ".format(end_date)

	if owner is None:
		sqlstr_part5 = ''
	else:
		sqlstr_part5 = "owner = '{}'".format(owner)

	sqlstr = sqlstr_part1+sqlstr_part2

	if len(sqlstr_part2)<1:
		sqlstr = sqlstr + sqlstr_part3
	elif len(sqlstr_part3)>1:
		sqlstr = sqlstr + 'and ' + sqlstr_part3
	
	if len(sqlstr_part2)<1 and len(sqlstr_part3)<1:
		sqlstr = sqlstr + sqlstr_part4
	elif len(sqlstr_part4)>1:
		sqlstr = sqlstr + 'and ' + sqlstr_part4

	if len(sqlstr_part2)<1 and len(sqlstr_part3)<1 and len(sqlstr_part4)<1:
		sqlstr = sqlstr + sqlstr_part5
	elif len(sqlstr_part5)>1:
		sqlstr = sqlstr + 'and ' + sqlstr_part5

	sqlstr_part6 = 'species != "tester" and id in (select max(id) id from met.massview group by cast(mass_date AS DATE),subjid)'
	if len(sqlstr_part2)<1 and len(sqlstr_part3)<1 and len(sqlstr_part4)<1 and len(sqlstr_part5)<1:
		sqlstr = sqlstr + sqlstr_part6
	elif len(sqlstr_part6)>1:
		sqlstr = sqlstr + 'and ' + sqlstr_part6
	
	df = pd.read_sql_query(sqlstr, DBE)
	df.to_csv('mass_to_pyrat.csv',index=False)

def export_today_mass_to_pyrat_csv():
	sqlstr = 'select pyratid as Identifier,cast(mass_date AS DATE) as Date,mass as Weight from met.massview where to_days(now())=to_days(mass_date) and species != "tester" and id in (select max(id) id from met.massview group by cast(mass_date AS DATE),subjid)'
	df = pd.read_sql_query(sqlstr, DBE)
	df.to_csv('today_mass_to_pyrat.csv',index=False)

def massToDB(mass, subjid, experid):
    ''' save mass to DB'''
    dbc.call('met.addMassReading(%s, %s, %s)', (subjid, float(mass), experid))
    dbc.commit()

def getAnimalInfo(rfid):
    sqlstr = (f'select subjid,pyratid,cageid,expgroup,owner from animals where rfid = "{rfid}"')
    sqlo = dbc.query(sqlstr)
    return sqlo

def get_lastest_mass_baseline_id_today(subjid):
	'''
	To mark animals for water restriction, you need to pick the lastest free water mass and update it to
	met.water_restriction_baseline using the sql procedure `call addBaselineMass(subjid, massid)`
	'''
	dbc.use('met')
	sqlstr = f'''
    with today_mass as
	(select massid from mass where subjid = "{subjid}" and CAST(mdate AS DATE) = DATE(NOW()))
	select max(massid) as mid from today_mass
    '''
	sqlo = dbc.query(sqlstr)
	if len(sqlo)==1:
		massid = sqlo[0][0]
	else:
		massid = 0
	return massid

def update_mass_with_lastest_massid(subjid,massid):
	sqlstr = (f'met.addBaselineMass("{subjid}",{massid:d})')
	dbc.call(sqlstr)

def update_animal_baseline(subjid):
	if isinstance(subjid,list):
		output_mess = 'ok'
		for subj in subjid:
			mess = update_animal_baseline(subjid=subj)
			if len(mess)>3: #this is an error message
				output_mess = mess
		return	output_mess
	massid = get_lastest_mass_baseline_id_today(subjid)
	if massid is None:
		message = f'{subjid} does not have mass reading today, please weigh!'
		print(message)
	else:
		update_mass_with_lastest_massid(subjid,massid)
		message = 'ok'
	return message


def update_turn_down_log(subjid,turn,turned_to,name,nturn_ts=None,experid=None,probeid=None,notes=None):
	'''
	subjid, subject id
	probe id, which probe on animal
	turn_ts, time of date
	turn, how many turns, like 0.5 turn or 1.5 turn
	turned to, the clock location of current point
	experid, who made the turn
	notes, if anything special that we want to keep notes
	'''
	dbc.use('phy')
	out = dbc.query('select probeid from probes where subjid=%s',subjid)
	if len(out)==1:
		probeid=out[0][0]
	dbc.use('met')
	experid=dbc.query('select experid from experimenters where Firstname=%s',name)[0][0]

	dbc.use('phy')
	currenttime = str(dbc.query('select now()')[0][0])
	if turn_ts is None:
		turn_ts=currenttime

	sqlstr = 'insert into turn_town_log (subjid,probeid,turn_ts,turn,turned_to,experid,turned_by,notes) values (%s, %s, %s, %s, %s, %s,  %s, %s)'

	dbc.execute(sqlstr, (subjid, probeid,turn_ts,turn,turned_to,experid,name,notes))
	#notify('The status of {} was set to {} by {}'.format(subjid, status, expername))

def retire_animal(subjid, water = False, experid=0):
	'''
	put animal to free water, no training and remove from schedule
	:retire_animal(subjid, water = False, experid=0)
	:water - stop water restriction or not, default False(not removing water restriction because of their cagemate)
	:param subjid of animal
	:return: none
	'''
	if isinstance(subjid, list):
		for sx in subjid:
			retire_animal(sx, water=water, experid=experid)
		return
		
	if water:
		change_animal_water_status(subjid=subjid, status='free', experid=experid)
	change_animal_status(subjid=subjid, status='free', experid=experid)
	remove_from_schedule(subjid) 
	add_subject_comment(subjid,'retired', experid)

def remove_from_schedule(subjid, fromdate=None, experid=0):
	'''
	remove from schedule
	:param subjid of animal
	:param fromdate defaults to now. But allows removal in the future.
	:param experid so we know who made the change.
	:return: none
	'''

	experid, expername = get_experid_from_info(experid)

	if fromdate is None:
		from time import strftime
		fromdate = strftime("%Y-%m-%d %H:%M:%S")
		print(fromdate)

	if isinstance(subjid,list):
		for subj in subjid:
			remove_from_schedule(subjid=subj, fromdate=fromdate, experid=experid)
		return

	dbc.call('remove_from_schedule("{}","{}")'.format(subjid, fromdate))
	notify('{} was removed from the schedule after {} by {}'.format(subjid, fromdate, expername))

def change_animal_status(subjid, status, experid=0, by_rfid=False, stattime=None,cmt=None):
	'''change_animal_status(subjid, status, experid=0, by_rfid=False, stattime=None)

		Changes the status of an animal.
		Inputs:
		subjid      The subject id
		status      One of: 'ordered','arrived','dead','ready','running','free'.
		experid     [0], To find out your experid try `select experid, firstname, lastname from experimenters`
		byrfid      [False], if set to True, interprets the subjid as an RFID.
		stattime    [now()], by default the change is status is the current time.
								If you are adding old data (e.g. from a lab notebook) put the time of the change.
		cmt         None by default, only need to comment for dead animals

	'''

	# Generally when we change the status we use the current time.
	# But if an experimenter made a change and forgot to update the DB, this option
	# allows them to specify the old time.
	if stattime is None:
		# Use the database time to avoid issues with timezones or incorrect local clocks
		stattime = str(dbc.query('select now()')[0][0])

	experid, expername = get_experid_from_info(experid)


	# if a list of IDs is passed in, then process each of them.
	if isinstance(subjid,list):
		for subj in subjid:
			change_animal_status(subj, status, experid, by_rfid, stattime)
		return

	if by_rfid:
		out = dbc.query('select subjid from animals where rfid=%s and status<>"dead"', (subjid, ))
		assert out and len(out) == 1 and len(out[0]) == 1, 'This RFID does not uniquely match a living animal'
		subjid = int(out[0][0])
		# Probably should make sure that rfid is unique and valid!

		# First check current status
	out = dbc.query('select status, ts from animal_status where subjid = %s and ts = (select max(ts) from animal_status where subjid=%s)', (subjid, subjid))
	if out:
		current_status, stat_ts = out[0]
		if status == current_status: # You can't change the status to the same status
				print('{} was already set to {} on {}'.format(subjid, status, stat_ts))
				return
		elif current_status=="dead": # This animal is dead, you probably have the wrong subjid
				print('{} was already set to {} on {}'.format(subjid, status, stat_ts))
				print('You cannot change the status of a dead animal. {} {}'.format(subjid, stat_ts))
				return
		elif status == "ordered":  # You can't order an animal that is already in the database
				print('{} was already set to {} on {}'.format(subjid, status, stat_ts))
				return
		elif current_status != "ordered" and status=="arrived": # Only ordered animals can be set to arrived.
				print('{} was already set to {} on {}'.format(subjid, status, stat_ts))
				print('Only ordered animals can be set to arrived.')
				return


				# elif current_status != "arrived" and status == "arrived":
				#     print('{} was already set to {} on {}'.format(subjid, status, stat_ts))
				#     print('Only ordered animals can be set to arrived.')
				#     return
	else:
				# This animal is not in the DB
		if status != "ordered":
				print('{} is not yet in the database.'.format(subjid))
				print('Only ordered animals can be set to arrived.')
				return

		# If animal is dead, you must explain the cause of death
	if status == "dead":
			assert cmt, 'You must explain how/why the animal died. e.g.' + \
					'(found dead in cage/ non-survival surgery/ euthanized by ISO overdose at end of experiment) '
			add_subject_comment(subjid, cmt, experid)

	sqlstr = 'insert into animal_status (subjid, experid, status, ts) values (%s, %s, %s, %s)'
	dbc.execute(sqlstr, (subjid, experid, status, stattime))
	notify('The status of {} was set to {} by {}'.format(subjid, status, expername))

def duplicate_expgroup(expgroupid, new_group_name, group_description='', experid=0):
	'''
		duplicate_expgroup(expgroupid, new_group_name, group_description='', experid=0)
		returns the expgroupid of the new group.
	'''
	experid, expername = get_experid_from_info(experid)

	if not isinstance(expgroupid, int):
		oldname = expgroupid
		expgroupid = dbc.query('select expgroupid from expgroups where egname=%s',(oldname,))
		assert expgroupid, "Could not find a group with name {}".format(oldname)
		expgroupid = expgroupid[0][0]

	# Check new group name is not taken
	is_taken = dbc.query('select expgroupid from expgroups where egname=%s',(new_group_name,))
	assert not is_taken, "{} is already used.".format(new_group_name)

	dbc.execute('insert into met.expgroups (egname, description) values (%s, %s)', (new_group_name, group_description))
	newid = dbc.query('select last_insert_id()')[0][0]
	dbc.execute(('insert into met.settings (expgroupid, stage, data, settingsname, comment, protocol, saved_by_experid)'
				' select %s, stage, data, settingsname, comment, protocol, %s from settings where expgroupid=%s'),
				(newid, experid, expgroupid))
	return newid

def get_settings_for_expgroup(expgroupid):
	'''
		get_settings_as_dict(expgroupid)
		get_settings_as_dict(expgroupname)
		returns a dictionary? or a pandas thing?

	'''
	if not isinstance(expgroupid, int):
		expgroupid = dbc.query('select expgroupid from expgroups where egname=%s',(expgroupid))




def change_subj_water_status_by_cage(cageid, status, experid=0):
	'''change_cage_water_status(cageid, status, experid=0)
	Change the entire cage's water status
	'''
	experid, expername = get_experid_from_info(experid)
	sqlstr = 'select subjid from met.animals where cageid = {}'.format(cageid)
	setout = dbc.query(sqlstr)
	for subjid in setout:
		change_animal_water_status(subjid[0], status)
	notify('The water status of cage {} was set to {} by {}'.format(cageid, status, expername))


def change_cage_water_status(cageid=None,pyratcageid=None, status=None, experid=0):
	'''change_cage_water_status(cageid=None,pyratcageid=None, status=None, experid=0)
	Change the entire cage's water status (waterin/waterout)
	'''
	experid, expername = get_experid_from_info(experid)
	if pyratcageid is None:
		sqlstr = 'insert into cage_water_status (status, cageid, experid) values (%s, %s, %s)'
		dbc.execute(sqlstr, (status, cageid, experid))
	else:
		sqlstr = 'select cageid from met.current_animal_cage where pyratcageid = "{}"'.format(pyratcageid)
		setout = dbc.query(sqlstr)
		cageid = setout[0][0]
		sqlstr = 'insert into cage_water_status (status, cageid, experid) values (%s, %s, %s)'
		dbc.execute(sqlstr, (status, cageid, experid))
	notify('The water status of cage {} was set to {} by {}'.format(cageid, status, expername))
	

def change_animal_water_status(subjid, status, experid=0, by_rfid=False, stattime=None):
	'''change_animal_water_status(subjid, status, experid=0, by_rfid=False, stattime=None)

	Changes the water status of an animal.
	Inputs:
	subjid      The subject id
	status      One of: 'free','controlled'.
	experid     [0], Put your first name and it will find the correct id
	byrfid      [False], if set to True, interprets the subjid as an RFID.
	stattime    [now()], by default the change is status is the current time.
				If you are adding old data (e.g. from a lab notebook) put the time of the change.

	'''

	# Generally when we change the water_status we use the current time.
	# But if an experimenter made a change and forgot to update the DB, this option
	# allows them to specify the old time.
	assert status == 'free' or status == 'controlled', 'The status is invalid. Please retry.'

	if stattime is None:
		# Use the database time to avoid issues with timezones or incorrect local clocks
		stattime = str(dbc.query('select now()')[0][0])

	experid, expername = get_experid_from_info(experid)


	# if a list of IDs is passed in, then process each of them.
	if isinstance(subjid,list):
		for subj in subjid:
			change_animal_water_status(subj, status, experid, by_rfid, stattime)
		return

	if by_rfid:
		out = dbc.query('select subjid from animals where rfid=%s and status<>"dead"', (subjid, ))
		assert out and len(out) == 1 and len(out[0]) == 1, 'This RFID does not uniquely match a living animal'
		subjid = int(out[0][0])
		# Probably should make sure that rfid is unique and valid!

	# First check current status
	out = dbc.query('select status, ts from animal_water_status where subjid = %s and ts = (select max(ts) from animal_water_status where subjid=%s)', (subjid, subjid))
	if out:
		current_status, stat_ts = out[0]
		if status == current_status:
			print('{} was already set to {} on {}'.format(subjid, status, stat_ts))
			return

	sqlstr = 'insert into animal_water_status (subjid, experid, status, ts) values (%s, %s, %s, %s)'
	dbc.execute(sqlstr, (subjid, experid, status, stattime))
	notify('The water status of {} was set to {} by {}'.format(subjid, status, expername))

def change_animal_health_status(subjid, status, experid=0, by_rfid=False, stattime=None):
	'''change_animal_health_status(subjid, status, experid=0, by_rfid=False, stattime=None)

	Changes the health status of an animal.
	Inputs:
	subjid      The subject id
	status      One of: 'weight','good','clinical','recovery','surgery'.
	byrfid      [False], if set to True, interprets the subjid as an RFID.
	stattime    [now()], by default the change is status is the current time.
				If you are adding old data (e.g. from a lab notebook) put the time of the change.

	'''


	allowed_status = ['weight', 'good', 'clinical', 'recovery', 'surgery']
	if status not in allowed_status:
		print('Status is "{}" but must be one of {}'.format(status, allowed_status))
		return

	experid, expername = get_experid_from_info(experid)

	# Generally when we change the status we use the current time.
	# But if an experimenter made a change and forgot to update the DB, this option
	# allows them to specify the old time.
	if stattime is None:
		# Use the database time to avoid issues with timezones or incorrect local clocks
		stattime = str(dbc.query('select now()')[0][0])

	# if a list of IDs is passed in, then process each of them.
	if isinstance(subjid,list):
		for subj in subjid:
			change_animal_health_status(subj, status, experid, by_rfid, stattime)
		return

	if by_rfid:
		out = dbc.query('select subjid from animals where rfid=%s and status<>"dead"', (subjid, ))
		assert out and len(out) == 1 and len(out[0]) == 1, 'This RFID does not uniquely match a living animal'
		subjid = int(out[0][0])
		# Probably should make sure that rfid is unique and valid!

	# First check current status
	out = dbc.query('select status, ts from animal_health_status where subjid = %s and ts = (select max(ts) from animal_health_status where subjid=%s)', (subjid, subjid))
	if out:
		current_status, stat_ts = out[0]
		if status == current_status:
			print('{} was already set to {} on {}'.format(subjid, status, stat_ts))
			return

		sqlstr = 'insert into animal_health_status (subjid, status, ts) values (%s, %s, %s)'
		dbc.execute(sqlstr, (subjid, status, stattime))
		notify('The status of {} was set to {} by {}'.format(subjid, status, expername))
		if status != 'good':
				change_animal_water_status(subjid, 'free', experid, by_rfid=False, stattime=None)
	else:
		sqlstr = 'insert into animal_health_status (subjid, status, ts) values (%s, %s, %s)'
		dbc.execute(sqlstr, (subjid, status, stattime))
		notify('The status of {} was set to {} by {}'.format(subjid, status, expername))


def put_on_recovery(subjid, comment, experid=0, stattime=None):
	'''put_on_recovery(subjid, surgery_info=None, experinfo=0, stattime=None)
		 Function to put animals on recovery surgeries
		 Inputs:
		 subjid           The subjid.
		 experinfo        The name or experid of the surgeon
		 stattime         The time of the surgery [now]

		 Note: this function will log the surgery in the animal_health_status table and then the animal will be set to recovery for 5 days and then will get set to good.

		 e.g. put_on_recovery(666, 'Jeff','2018-4-4 10:24:00')
	'''
	if not stattime:
		from time import strftime
		stattime = strftime("%Y-%m-%d %H:%M:%S")

	if isinstance(subjid, int):
		subjid = [subjid,]

	if len(subjid) != 1:
		print('Please log one surgery at a time.')
		return

	# Add surgery to health status
	change_animal_health_status(subjid, 'recovery', experid, stattime=stattime)
	# Add a surgery comment
	add_subject_comment(subjid[0], f'Placed on recovery for: {comment}', experid)
	# Add recovery to health status (one day later)
	nextday = str(datetime.strptime(stattime, '%Y-%m-%d %H:%M:%S') + timedelta(days=1))
	change_animal_health_status(subjid, 'recovery', experid, stattime=nextday)
	sixday = str(datetime.strptime(stattime, '%Y-%m-%d %H:%M:%S') + timedelta(days=6))
	change_animal_health_status(subjid, 'good', experid, stattime=sixday)
	change_animal_water_status(subjid,'controlled',experid,stattime=sixday)

	# Check if animal is on schedule
	out = dbc.query('select * from met.schedule_today where subjid = {}'.format(subjid[0]))
	if out:
		# Get schedule info
		today = datetime.today().strftime('%Y-%m-%d')
		twoday = (datetime.today() + timedelta(days=2)).strftime('%Y-%m-%d')
		sixday = (datetime.today() + timedelta(days=6)).strftime('%Y-%m-%d')
		out = dbc.query('select * from met.schedule where subjid = {} and slotdate = "{}"'.format(subjid[0],today))[0]
		slottime = str(out[2])

		# Insert blank line in two day's schedule (becasue of the unique key in SQL has already been taken by nextday)
		S = {'rigid':out[0],
				'slotdate':twoday,
				'slottime':slottime,
				'comments':'Reserved for {}'.format(subjid[0]),
				'slotid':out[7]}

		dbc.save('schedule',S)

		# Insert a proper schedule in 6 days
		S = {'rigid':out[0],
				'slotdate':sixday,
				'slottime':slottime,
				'subjid': subjid[0],
				'slotid':out[7]}

		dbc.save('schedule',S)
		
		print('The current slot of {} in rig {} at {} is reserved until {} and will be added onto schedule on that day'.format(
		subjid[0],out[0],slottime,sixday))
	else:
		print('Subject {} is not on schedule.'.format(subjid[0]))

	# Add surgery info to surgery_log table
	if surgery_info:
		print("Sorry, saving the surgery details is not implemented yet")


def log_surgery(subjid, surgery_info=None, experid=0, stattime=None):
	'''log_surgery(subjid, surgery_info=None, experinfo=0, stattime=None)
		 Function to log surgeries
		 Inputs:
		 subjid           The subjid.
		 surgery_info     A dictionary with the surgery info. (See below)
		 experinfo        The name or experid of the surgeon
		 stattime         The time of the surgery [now]

		 Note: this function will log the surgery in the animal_health_status table and then the animal will be set to recovery for 5 days and then will get set to good.

		 e.g. log_surgery(666, None, 'Jeff','2018-4-4 10:24:00')
	'''
	if not stattime:
		from time import strftime
		stattime = strftime("%Y-%m-%d %H:%M:%S")

	if isinstance(subjid, int):
		subjid = [subjid,]

	if len(subjid) != 1:
		print('Please log one surgery at a time.')
		return

	# Add surgery to health status
	change_animal_health_status(subjid, 'surgery', experid, stattime=stattime)
	# Add a surgery comment
	add_subject_comment(subjid[0], 'surgery today', experid)
	# Add recovery to health status (one day later)
	nextday = str(datetime.strptime(stattime, '%Y-%m-%d %H:%M:%S') + timedelta(days=1))
	change_animal_health_status(subjid, 'recovery', experid, stattime=nextday)
	sixday = str(datetime.strptime(stattime, '%Y-%m-%d %H:%M:%S') + timedelta(days=6))
	change_animal_health_status(subjid, 'good', experid, stattime=sixday)
	change_animal_water_status(subjid,'controlled',experid,stattime=sixday)

	# Check if animal is on schedule
	out = dbc.query('select * from met.schedule_today where subjid = {}'.format(subjid[0]))
	if out:
		# Get schedule info
		today = datetime.today().strftime('%Y-%m-%d')
		twoday = (datetime.today() + timedelta(days=2)).strftime('%Y-%m-%d')
		sixday = (datetime.today() + timedelta(days=6)).strftime('%Y-%m-%d')
		out = dbc.query('select * from met.schedule where subjid = {} and slotdate = "{}"'.format(subjid[0],today))[0]
		slottime = str(out[2])

		# Insert blank line in two day's schedule (becasue of the unique key in SQL has already been taken by nextday)
		S = {'rigid':out[0],
				'slotdate':twoday,
				'slottime':slottime,
				'comments':'Reserved for {}'.format(subjid[0]),
				'slotid':out[7]}

		dbc.save('schedule',S)

		# Insert a proper schedule in 6 days
		S = {'rigid':out[0],
				'slotdate':sixday,
				'slottime':slottime,
				'subjid': subjid[0],
				'slotid':out[7]}

		dbc.save('schedule',S)
		
		print('The current slot of {} in rig {} at {} is reserved until {} and will be added onto schedule on that day'.format(
		subjid[0],out[0],slottime,sixday))
	else:
		print('Subject {} is not on schedule.'.format(subjid[0]))

	# Add surgery info to surgery_log table
	if surgery_info:
		print("Sorry, saving the surgery details is not implemented yet")


def sac_rfid(rfid, experid=0, stattime=None, cmt=None):
		'''sac_rfid(rfid, experid=0)
		Marks an animal as dead by RFID
		'''
		change_animal_status(rfid, 'dead', experid, by_rfid=True, stattime=stattime, cmt=cmt)

def sac_subjid(subjid, experid=0, stattime=None, cmt=None):
		'''Mark an animal as dead by subjid
		'''
		change_animal_status(subjid, 'dead', experid, stattime=stattime, cmt=cmt)

def assign_to_experimenter(subjlist, experid):
	'''
	Each animal has an "owner". This owner is someone in the experimenters
	table. This function assigns a subject to a new owner.
	'''
	experid, expername = get_experid_from_info(experid)

	if not isinstance(subjlist,list):
		subjlist = [subjlist]

	for subjid in subjlist:
		dbc.execute('insert into animal_owner_status (subjid, experid) values (%s, %s)', (subjid, experid))

	notify("{} animals assigned to {}".format(len(subjlist), expername))

def assign_one_to_experimenter(subjid, experid):
	'''
	Each animal has an "owner". This owner is someone in the experimenters
	table. This function assigns a subject to a new owner.
	'''
	experid, expername = get_experid_from_info(experid)

	dbc.execute('insert into animal_owner_status (subjid, experid) values (%s, %s)', (subjid, experid))

	#notify("{} animals assigned to {}".format(len(subjlist), expername))

def notify(msg):
	title = "Manage Animals Notifications"
	try:
		net.send_slack_msg(title,msg,net.get_slack_url(),BPOD_BOT_CHANNEL_ID)
	except Exception:
		print('Failed to send slack')
	print(msg)

def add_subject_comment(subjid, cmt, experid=0, ts=None):
	'''
	add_subject_comment(subjid, cmt, experimenter = gitlab user)

	saves a subject comment to the met.commentlogger table with reference to met.animals
	'''
	
	if experid == 0:
		[experid, expername] = get_experid_from_info(experid)

	if ts is None:
		ts = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	
	if isinstance(subjid,list):
		for sx in subjid:
			add_subject_comment(subjid=sx, cmt=cmt, experid=experid,ts=ts)
		return	
	assert isinstance(subjid, str), 'subjid must be an string.'
	assert isinstance(cmt, str) and len(cmt) > 0 and len(cmt) < 1000, 'comment should be a not empty string and less than 1000 characters.'
	S = {'ts': ts, 
		 'subjid':subjid,
		 'experid':experid,
		 'comment':cmt}
	dbc.save('subject_comment',S)

def add_rig_comment(rigid, cmt, experid=0):
	'''
	add_rig_comment(rigid, cmt, experimenter = gitlab user)

	saves a rig comment to the met.commentlogger table
	'''
	if experid == 0:
		[experid, expername] = get_experid_from_info(experid)

	assert isinstance(rigid, int), 'rig must be an integer.'
	assert isinstance(cmt, str) and len(cmt) > 0 and len(cmt) < 1000, 'comment should be a not empty string and less than 1000 characters.'
	S = {'table':'met.rigs',
		 'tableid':rigid,
		 'experid':experid,
		 'comment':cmt}
	dbc.save('commentlogger',S)


def get_session_comments(sessid):
		'''
				get_session_comments(sessid)
				Returns a pandas dataframe of comments for this session.
		'''
		out = pd.read_sql('select comment, ts as comment_time, firstname as commented_by from met.commentlogger join met.experimenters using (experid) where tableid=%s and `table` = "beh.sessions"',DBE,params=(sessid,))
		return out

def get_subject_comments(subjid):
	'''
				get_subject_comments(subjid)
				Returns a list of pandas dataframe of comments for this subject and comments related to sessions for this subject.
	'''

	sessout = pd.read_sql('''select comment, tableid as sessid, ts as comment_time, firstname as commented_by
							 from ((met.commentlogger c join met. experimenters e using (experid))
							 join beh.sessions b on (`table` = "beh.sessions" and c.tableid=b.sessid)) 
							 where subjid=%s;''',
							 DBE, params = (subjid,))

	subjout = pd.read_sql('select comment, ts as comment_time, firstname as commented_by from met.commentlogger join met.experimenters using (experid) where tableid=%s and `table` = "met.animals"',DBE,params=(subjid,))

	return (subjout, sessout)


def add_session_comment(sessid, cmt, experid=0):
	'''
	add_session_comment(sessid, cmt, experimenter = gitlab user)

	saves a session comment to the met.commentlogger table with reference to the beh.sessions table
	'''
	if experid == 0:
		[experid, expername] = get_experid_from_info(experid)

	assert isinstance(sessid, int), 'sessid must be an integer.'
	assert isinstance(cmt, str) and len(cmt)>0, 'comment should be a not empty string.'

	S = {'table':'beh.sessions',
		 'tableid':sessid,
		 'experid':experid,
		 'comment':cmt}
	dbc.save('commentlogger',S)

def getSettings(subjid, settingstime=None):
		'''
		getSettings(subjid, settingstime=None)
		not implemented because of the matlab/json stuff.
		'''
		pass
		# return
		# if settingstime is None:
		# 		sqlstr = 'select * from met.subject_settings where subjid = {} order by settings_date desc limit 1';
		# 		setout = dbc.query(sqlstr, (subjid,));
		# else:
		# 		sqlstr = 'select * from met.subject_settings where subjid = {} and date(settings_date)={}';
		# 		setout = dbc.query(sqlstr, (subjid, settingstime));
		# end



def get_experid_from_info(exper_info=0):
	'''
	get_experid_from_info(exper_info):
	input:
	a string that will match something about an experimenter. Could be firstname, gitlabuser, or netid
	output:
	the ID for that experimenter in the database.
	e.g. id = get_experid_from_info('jerlich') # returns 10
	'''
	if isinstance(exper_info, int):
		# Try to load from dbconf
		# Try by gitlab_username
		if exper_info == 0:
			try:
				import subprocess as sp
				out = sp.check_output(['git', 'config', 'user.name'])
				exper_info = out.split(b' ')[0].decode()
			except:
				print("Unexpected error:", sys.exc_info()[0])
				return (0, '')
		else:
			eid = dbc.query('select experid, concat(firstname," ",lastname) as name from experimenters where experid = {}'.format(exper_info))
			return int(eid[0][0]), str(eid[0][1])


	eid = dbc.query('select experid, concat(firstname," ",lastname) as name from experimenters where Firstname like "{}%"'.format(exper_info))
	if len(eid) == 1:
		return int(eid[0][0]), str(eid[0][1])

	eid = dbc.query('select experid, concat(firstname," " ,lastname) as name from experimenters where gitlabuser=%s', (exper_info, ))
	if len(eid) == 1:
		return int(eid[0][0]), str(eid[0][1])

	eid = dbc.query('select experid, concat(firstname," ",lastname) as name from experimenters where netid=%s', (exper_info, ))
	if len(eid) == 1:
		return int(eid[0][0]), str(eid[0][1])

	raise AssertionError('Could not match a unique experimenter to the supplied information')

def set_subject_stage(subjlist, stage=1, experid=0, clear_subject_settings=False):
	'''
	set_subject_stage(subjlist, stage=1):
	Input:
	subjlist        a list of subjids.
	stage           a stage (e.g. from the setting table) to set them to.
	experinfo       the id or name of an experimenter who is making the change
	clear_subject_settings  [False] Normally, the most recent subj_data will be copied to the new stage. setting this to True will clear settings.
	You might use this to jump back to an earlier stage or to skip ahead.
	'''

	experid, expername = get_experid_from_info(experid)

	if not isinstance(subjlist,list):
		subjlist = [subjlist]

	for subjid in subjlist:
		# Get the current settings, including the customized subject data.
		sqlstr = 'select subjid, subj_data, protocol, settingsname, stage from current_settings where subjid = %s'
		current_settings = dbc.query(sqlstr, (subjid, ), as_dict=True)
		# Get the settingid for the new stage, based on the experimental group of the subject
		sqlstr = 'select a.settingsid, protocol, settingsname, stage from settings a, animals b where b.subjid=%s and a.expgroupid=b.expgroupid and a.stage=%s'
		newsetting = dbc.query(sqlstr, (subjid, stage), as_dict=True)

		if newsetting is None or len(newsetting) == 0:
			print('It seems that {} has not yet been assigned to an experimental group. Please do so before running this function'.format(subjid))
			return

		newdict = {'subjid': subjid,
					 'settingsid': newsetting['settingsid'],
					 'saved_on_ip': net.getIP()}
		if experid != 0:
			newdict['saved_by_experid'] = experid

		if len(current_settings) > 0: # This is an old animal
			if clear_subject_settings:
				comment = 'and data was cleared.'
			else:
				comment = 'and subj_data was copied.'
				newdict['settings_data'] = current_settings['subj_data']

		dbc.save('subject_settings', newdict)
		if current_settings == {}:
			oldstr = ('initializing stage setting for subjid:'+str(subjid)+' ')
			comment = ''
		else:
			oldstr = 'Subject {subjid} moved from {protocol} stage {stage} ({settingsname}) to '.format(**current_settings)
		newstr = '{protocol} stage {stage} ({settingsname}) '.format(**newsetting)
		notify(oldstr + newstr + comment)
		# note: using something like pandas, we could probably make this more efficient, but generally not worth it.

def create_cage(subjlist, experid, pyratcageid, stattime=None, water_status='in'):
	'''create_cage(subjlist, experid)
		 Input:
		 subjlist is a subjid or a list of subjids
		 experid is either an int or string of netid or name
		 creates a new cage for those animals and returns the cageid
	'''
	if not isinstance(subjlist,list):
		subjlist = [subjlist]

	pyratcageid = pyratcageid.strip() #remove space

	experid, expername = get_experid_from_info(experid)


	subjstr = str(subjlist)[1:-1]
	# 1st Check: all animals are alive
	out = dbc.query('select subjid from animals where (datedead is not NULL) and subjid in ({})'.format(subjstr))
	assert not out, 'You cannot put a subject in a cage who is dead: {}'.format(str(out[0])[1:-1])

	# 2nd Check: all animals are arrived
	out = dbc.query('select subjid from animals where (datearrived is NULL) and subjid in ({})'.format(subjstr))
	if out:
		mark_arrived(subjlist,experid)

	# 3rd check: all animals belong in the same cage
	sqlstr = 'select count(distinct(species)), count(distinct(ppl)), count(distinct(gender)) '\
				'from animals '\
				'where subjid in ({})'.format(subjstr)

	species_cnt, protocol_cnt, gender_cnt = dbc.query(sqlstr)[0]
	assert species_cnt > 0, "Subject must be marked arrived before they can be assigned to a cage."
	assert species_cnt == 1, "Only subjects of the same species can be housed together"
	assert protocol_cnt == 1, "Only subjects on the same IACUC protocol can be housed together"

	# 4th check: pyrat cageid are not been assigned yet
	sqlstr = 'select * from animal_cage_status where pyratcageid="{}"'.format(pyratcageid)
	out = dbc.query(sqlstr)
	assert len(out) < 1, "This pyratcageid already exists, please use move-cage tab to add animals to this cage"

	if gender_cnt != 1:
		notify('Male and female subjects are being placed in the same cage: {}'.format(subjstr))

	species = dbc.query('select species from animals where subjid in ({})'.format(subjstr))[0][0]
	last_cageid = dbc.query('select max(cageid) from animals where species=%s', (species, ))[0][0]
	if species == 'mouse' and last_cageid is None:
		last_cageid = 0
	elif species == 'rat' and last_cageid is None:
		last_cageid = 100
	cageid = last_cageid + 1
	if cageid // 100 - last_cageid // 100 > 0:
		cageid += 100

	if stattime is None:
		# Use the database time to avoid issues with timezones or incorrect local clocks
		stattime = str(dbc.query('select now()')[0][0])
	for subject in subjlist:
		dbc.execute("insert into animal_cage_status (subjid,ts,cageid,pyratcageid) values (%s, %s, %s, %s)", (subject, stattime, cageid,pyratcageid))

	dbc.call("waterIn({},{})".format(cageid, experid))
	# change status from ordered to arrived automatically
	return cageid


def assign_to_cage(subjlist, pyratcageid, cageid, experid=None, stattime=None):
	'''assign_to_cage(subjlist, cageid, experid=None, stattime=None)
		 Input:
		 subjlist is a subjid or a list of subjids
		 cageid is an int
		 experid is either an int or string of netid or name
		 stattime is the time that changes are made.
		 Creates a new cage for those animals and returns the cageid
	'''
	if not isinstance(subjlist,list):
		subjlist = [subjlist]
	if experid:
		experid, expername = get_experid_from_info(experid)
	there_is_such_cage = dbc.query("select 1 from current_animal_cage where cageid=%d limit 1" % (cageid,))
	if stattime is None:
		# Use the database time to avoid issues with timezones or incorrect local clocks
		stattime = str(dbc.query('select now()')[0][0])
	#for subject in subjlist:

	if len(there_is_such_cage) == 0:
		print("There is no cage with id {}. Please use create_cage instead".format(cageid))
		return
		# there_is_such_cage is [[1]] or [[]]
	for subject in subjlist:
		dbc.execute("insert into animal_cage_status (subjid,ts,cageid,pyratcageid) values (%s, %s, %s, %s)", (subject, stattime, cageid,pyratcageid))

	out = dbc.query("select water from animals where cageid=%s", (cageid, )) # returns a list of tuples
	for item in out:
		if item != out[0]:
			print("Warning: animals with different water status are put together!")

def assign_expgroup(subjlist, expgroupid=None):
	'''
		assign_expgroup(subjlist, expgroupid=None)
		Input:
		subjlist    a list of subjids.
		expgroupid  an id from the expgroups table.  If this is not passed in
				we ask for input.
	'''
	if not isinstance(expgroupid, int):
		# Try by gitlab_username
		print("Need to give experimental group.")
		out = dbc.query('select * from expgroups order by expgroupid')
		for exid, expname, expdesc in out:
				print("{}\t {}\t {}".format(exid, expname, expdesc))
		return
	else:
		out = dbc.query('select * from expgroups where expgroupid=%s',(expgroupid,))
		if not out:
			print("This experimental group doesn't exist. Please create one first.")
			return


	if not isinstance(subjlist,list):
		subjlist = [subjlist]

	for subjid in subjlist:
		# Is the animal alive?
		current_status = get_subject_status(subjid)
		if current_status == "dead":
			print("{} is dead. Please double check your subjid".format(subjid))
		else:
			if current_status != "running":
				print("Warning: {} is not currently set to running.".format(subjid))

			current_expgroupid = get_subject_expgroup(subjid)[0]

			if current_expgroupid == 0: # No current group
				dbc.execute('insert into animal_expgroup_status (subjid, expgroupid) values (%s, %s)', (subjid, expgroupid))
				notify("{} assigned to group {}".format(subjid, expgroupid))
				set_subject_stage(subjid, 1)
			else:
				dbc.execute('insert into animal_expgroup_status (subjid, expgroupid) values (%s, %s)',
					(subjid, expgroupid))
				notify("{} moved from group {} to group {}. Remeber to set subject stage.".format(subjid, current_expgroupid, expgroupid))

def get_subject_expgroup(subjid):
	out = dbc.query('select expgroupid, expgroup from animals where subjid = %s',
						 (subjid, ))
	try:
		return out[0]
	except IndexError:
		return(0, 'Subject {} either does not exist or has not been assigned to an experimental group'.format(subjid))


def get_subject_status(subjid):
	out = dbc.query('select status from animals where subjid = %s',(subjid,))
	try:
		return out[0][0]
	except IndexError:
		print('Subject {} does not exist'.format(subjid))
		return ''

def subject_summary(subjlist):
	pass


def mark_arrived(subjids=[], experid=0, arrived_on=None):
	'''
		Use this function with no inputs to make all ordered animals as arrived.
		If you forgot to run it on the day of arrival specify arrived_on like:
		mark_arrived(arrived_on='2019-08-20')
	'''
	if not subjids:
		ordered_animals = dbc.query('select subjid from animals where status="ordered"')
		subjids = [x[0] for x in ordered_animals]

	change_animal_status(subjids, 'arrived', experid=experid, stattime=arrived_on)
	change_animal_water_status(subjids, 'free', experid=experid, stattime=arrived_on)
	

def tag_animal(subjid, rfid):
	dbrfid = dbc.query('select rfid from animals where subjid=%s',(subjid,))[0][0]
	assert dbrfid is None, "You cannot tag an animal that is already tagged. Please see administrator."
	dbc.call("met.tag_animal(%s,%s)",(subjid, rfid))
	dbrfid = dbc.query('select rfid from animals where subjid=%s',(subjid,))[0][0]
	assert rfid is not None, "RFID failed to insert."


def tag_animals_from_list(subjids, rfids):
	'''
		Tags a list of animals. Input a list of subjids and a list of rfids
	'''
	assert len(subjids)==len(rfids)
	for subjid, rfid in zip(subjids, rfids):
		tag_animal(subjid, rfid)

def experList():
	sqlstr = 'select experid,Firstname from experimenters where active=1 and tech=1'
	sqlo = dbc.query(sqlstr)
	return sqlo

def check_subject_id_format(subjid):
	if isinstance(subjid,list):
		for subj in subjid:
			result = check_subject_id_format(subj)
			if result is False:
				return False
		return True
	if len(subjid) != 10 or not isinstance(subjid,str):
		return False
	if not re.match(r"[A-Z][A-Z][A-Z]-[A-Z]-[0-9][0-9][0-9][0-9]",subjid):
		return False
	return True

def create_one_animal(animal_dict,experid,order_on=None,arrived_age=None):
	property_list = ['subjid','pyratid','species','strain', 'DOB','ppl', 'gender']

	#if not all(name in animal_dict for name in property_list):
	#	print('Your dictionary includes the following names:{}').format(animal_dict.keys())
	#	print('It must include these names :{}').format(property_list)

	ad = animal_dict

	# do checks on the animal_dict
	assert check_subject_id_format(ad['subjid'][0]), "Subject ID must be in the format AAA-A-0000"

	if not order_on:
		order_on = str(dbc.query('select now()')[0][0])

	if not arrived_age:
		arrived_age = "NULL"

	sqlstr = ('insert into animal_info '
				'(subjid, pyratid, species, strain, DOB, ppl, gender) '
				'values (%s, %s, %s, %s, %s, %s, %s)')
	
	#print('insert into animal_info (subjid, pyratid, species, strain, arrivedage, ppl, gender) values (%s, %s, %s, %s, %s, %s, %s)',ad['subjid'][0],ad['pyratid'][0],ad['species'][0],ad['strain'][0],ad['age'][0],ad['ppl'][0],ad['gender'][0])

	dbc.execute(sqlstr,
		(ad['subjid'][0],ad['pyratid'][0],ad['species'][0],
		ad['strain'][0],ad['DOB'][0],ad['ppl'][0],
	    ad['gender'][0]))

	#print(order_on)
	print("subject {} created in database".format(ad['subjid'][0]))
	notify("subject {} created in database".format(ad['subjid'][0]))
	change_animal_status(ad['subjid'][0],'ordered', experid=int(experid), stattime=order_on)
	change_animal_water_status(ad['subjid'][0],'free', experid=int(experid))

def order_animals(animal_dict, experid, ordered_on=None):
	property_list = ['species', 'strain', 'arrivedage',
	'iacucprotocol', 'gender', 'number_of_animals']
	if not all(name in animal_dict for name in property_list):
		print('Your dictionary includes the following names:{}').format(animal_dict.keys())
		print('It must include these names :{}').format(property_list)

	experid, expername = get_experid_from_info(experid)

	# num_left = check_strain_species_protocol(animal_dict)
	# eventually we should check how many animals are left
	ad = animal_dict
	if not ordered_on:
		ordered_on = str(dbc.query('select now()')[0][0])

	try:
		# Check if the number of animal reaches limit per year.
		year = int(ordered_on[:4])
		month = int(ordered_on[5:7])
		year2017 = dbc.query('select count(species) from animals where (year(dateordered) = 2017 and month(dateordered)>=7)')[0][0]
		year2018 = dbc.query('select count(species) from animals where (year(dateordered) = 2018 and month(dateordered)<7)')[0][0]
		# The caculation is still problematic.
		total_ordered_in_year2 = year2017 + year2018
		if total_ordered_in_year2 + ad['number_of_animals'] > 296:
			print("The maximum number of %s can be ordered (%d) has been reached." % (ad['species'],296))
			return
		# dbc.execute('lock table animal_info LOW_PRIORITY WRITE')
		last_subjid = dbc.query('select max(subjid) from animal_info where species=%s and iacucprotocol like "15-%%"',
			(animal_dict['species'],))[0][0]

		# We add the like "15-%" to ignore the rats from xiaoyue

		subjids = []
		for x in range(ad['number_of_animals']):
			subjid = x + last_subjid + 1
			# Validate subjid
			if subjid // 1000 - last_subjid // 1000 > 0:
				# mouse id starts from odd_number * 1000
				# rat id starts from even_number * 1000
				subjid += 1000

			# Check if IACUC match species match
			if not ((ad['species'] == "mouse" and ad['iacucprotocol'] == "15-1465")
				or (ad['species'] == "rat" and ad['iacucprotocol'] == "15-1464")
				or ad['species'] == "tester"):
				print("Species %s and IACUC protocol %s do not match." % (ad['species'],ad['iacucprotocol']))
				return

			# Check if species and strain match
			if not ((ad['species'] == "mouse" and ad['strain'] == "CD1")
				or (ad['species'] == "mouse" and ad['strain'] == "C57B6")
				or (ad['species'] == "rat" and ad['strain'] == "SD")
				or (ad['species'] == "rat" and ad['strain'] == "LE")
				or (ad['species'] == "rat" and ad['strain'] == "BN")
				or ad['species'] == "tester"):
				print("Species %s and strain %s do not match." % (ad['species'],ad['strain']))
				return

			sqlstr = ('insert into animal_info '
				'(subjid, species, strain, arrivedage, iacucprotocol, gender) '
				'values (%s, %s, %s, %s, %s, %s)')
			dbc.execute(sqlstr,
				(subjid, ad['species'], ad['strain'],
						 ad['arrivedage'], ad['iacucprotocol'],
						 ad['gender']))
			change_animal_status(subjid,'ordered', experid=experid, stattime=ordered_on)
			change_animal_water_status(subjid,'free', experid=experid)
			subjids.append(subjid)

		email_str =  """Please copy and send the following email to nyushanghai-laboratory-animal-resources@nyu.edu"
		
		Dear ACUP staff:
		
		Please order the following animals:"""
		for item in property_list:
			email_str += "\n" + item+": "+str(ad[item])
		email_str += "Please email me with the invoice after the vendor responds."
		print(email_str)
	except:
		tb.print_exc()

	finally:
		dbc.execute('unlock tables')
		return subjids


def check_strain_species_protocol(animal_dict):
	#valid_strains =
	pass

def change_valve_status(rigid, valve_num=1, stattime=None):
	'''change_valve_status(rigid, valve_num=1):

	Changes the valve status of the rig:
	Inputs:
	rigid         rigid, e.g. 101 
	valve_num   either 0, 1 or 2, indicating which valve number to use, 0 means no forced '''

	allowed_num = [0,1,2]
	if valve_num not in allowed_num:
		print('Valve number is "{}" but must be one of {}'.format(valve_num, allowed_num))
		return

	if stattime is None:
		# Use the database time to avoid issues with timezones or incorrect local clocks
		stattime = str(dbc.query('select now()')[0][0])

	# First check current valve number
	out = dbc.query('select valve_num, ts from current_forced_valve where rigid = %s)', (rigid))
	if out:
		current_num, stat_ts = out[0]
		if valve_num == current_num:
			print('The valve number of {} was already set to {} on {}'.format(rigid, valve_num, stat_ts))
			return

	sqlstr = 'insert into forced_valve_status (rigid, valve_num, ts) values (%s, %s, %s)'
	dbc.execute(sqlstr, (rigid, valve_num, stattime))
	notify('The valve number of {} was set to {} on {}'.format(rigid, valve_num, stattime))

def copy_settings(from_subjid, to_subjid, settings_date=None, experid=None, cmt="copy operation"):
		"""
		related tables:
				met.settings
				met.subject_settings
				met.animal_owner_status
				met.animal_exprgroup_status
				met.animal_status
		things to be copied:
				owner
				experimental group
				stage
				subject settings
				status
		"""
		# TODO: check what cause the need of adding a comma to the tuple to make the query work
		# TODO: check if exprgroup is consistant between table subject_settings(->settings) and animal_expgroup_status
		# it seems that settings is in the bottom, animal_expgroup_status should be changed based on that
		# Sanity check
		from_subj_exist=len(dbc.query('select * from current_animal_owner where subjid = %s order by ts asc',(from_subjid,)))>0
		to_subj_exist=len(dbc.query('select * from current_animal_owner where subjid = %s order by ts asc',(to_subjid,)))>0
		exper_exist=len(dbc.query("select experid from experimenters where experid = %s", (experid,)))>0
		if not from_subj_exist or not to_subj_exist:
			if not from_subj_exist:
				print("subject {} does not exist".format(from_subjid))
			if not to_subj_exist:
				print("subject {} does not exist".format(to_subjid))
			return
		if not exper_exist:
			print("experimenter id {} does not exist".format(experid))
			return
		experid, expername = get_experid_from_info(experid)
		cmt += 'by {}'.format(expername)
		target_owner=dbc.query('select experid from current_animal_owner where subjid = %s order by ts asc',(from_subjid,))[-1][0]
		target_status=get_subject_status(from_subjid)
		target_settings_lst=dbc.\
				query('select t1.settings_date as settings_date, '+
						't1.settingsid as settingsid, '+
						't1.settings_data as settings_data, '+
						't1.saved_by_experid as saved_by_experid, '+
						't1.saved_on_ip as saved_on_ip, '+
						't2.expgroupid as expgroupid, '+
						't2.stage as stage '+
						'from subject_settings as t1 join settings as t2 on t1.settingsid = t2.settingsid where subjid = %s order by t1.settings_date asc',(from_subjid,))
		if len(target_settings_lst)==0:
				print("the settings for subjid {} does not exist".format(from_subjid))
				return False
		if settings_date is None:
				index_selection=-1
		else:
				def find_a_matched_datetime(date, compare_list):
						for i in range(len(compare_list)):
								if date>compare_list[i]:
										if i==len(compare_list)-1:
												i+=1
										continue
								else:
										break
						if i==len(compare_list):
								# the setting_date is larger than the latest one
								return -1
						elif i==0:
								# the setting_date is smaller than the oldest one
								return 0
						else:
								# if compare_list[i]-date>date-compare_list[i-1]:
								#     return i-1
								# else:
								#     return i
								return i-1
				index_selection=find_a_matched_datetime(settings_date, [t_s[0] for t_s in target_settings_lst])
		tar_setting_date, tar_settingid, tar_settingsdata, _, _, tar_groupid, tar_stage=target_settings_lst[index_selection]
		change_animal_status(to_subjid, target_status, experid=experid, by_rfid=False, stattime=None,cmt=cmt) # satus change and add date and changed by whom
		if target_status!='dead':
			add_subject_comment(to_subjid, cmt, experid)
		assign_to_experimenter(to_subjid, target_owner) # owner assignment
		# assign_expgroup(to_subjid, expgroupid=tar_groupid) # assign to group
		# Because we want to copy, so the groupid need to assigned anyway
		def _force_assign_groupid(_subjid, _groupid):
				_current_expgroupid = get_subject_expgroup(_subjid)[0]
				if _current_expgroupid == 0: # No current group
						dbc.execute('insert into animal_expgroup_status (subjid, expgroupid) values (%s, %s)', (_subjid, _groupid))
						notify("{} assigned to group {}".format(_subjid, _groupid))
						set_subject_stage(_subjid, 1)
				else:
						dbc.execute('insert into animal_expgroup_status (subjid, expgroupid) values (%s, %s)',
								(_subjid, _groupid))
						notify("{} moved from group {} to group {}. Remeber to set subject stage.".format(_groupid, _current_expgroupid, _groupid))
		_force_assign_groupid(to_subjid, tar_groupid)
		# copy the settings
		newdict_subjsettings = {'subjid': to_subjid,
								'settingsid': tar_settingid,
								'settings_data':tar_settingsdata,
								'saved_by_experid':experid,
								'saved_on_ip': net.getIP()}
		dbc.save('subject_settings', newdict_subjsettings)
		notify("The subject {} settings is copied to subject {}".format(from_subjid, to_subjid))

def log_infusion(subjid, infusion_dict, experid=0, notes=None, ignore_sess=0, ignore_reason=None):
	'''log_infusion(subjid, infusion_dict, experid=0, notes=None, ignore_sess=0, ignore_reason=None)
		 Input:
		 subjid is subjid, so far list is not supported
		 infusion_dict should contain the following fields:
		 {'iso_start_time': "2020-07-10 14:21:00", # can be None if no ISO was used
		  'iso_end_time': "2020-07-10 14:31:00", # can be None if no ISO was used
		  'infusion_start_time': "2020-07-10 14:25:00", # the time when infusion started, cannot be None
		  'infusion_end_time': "2020-07-10 14:30:00", # the time when injector was pulled out, cannot be None
		  'region': 'LPPC', # the region of infusion, e.g. LPPC, RFOF, LPPC+RPPC
		  'volume': 0.3, # volume infused in µl
		  'concentration': 1, # drug concentration in µg/µl
		  'drug':'muscimol', # the drug name
		  'performed_by':'Jeff'}
		 experid: your experid or name
		 notes: any comments go here
		 ignore_sess: 0 or 1
		 ignore_reason: if ignore_sess = 1, you must provide ignore_reason
	'''
	infusion_list = ['iso_start_time', 'iso_end_time',
					  'infusion_start_time', 'infusion_end_time',
					  'region', 'volume', 'concentration', 'drug','performed_by']
	
	if not all(name in infusion_dict for name in infusion_list):
		print('Your dictionary includes the following names:{}').format(infusion_dict.keys())
		print('It must include these names :{}').format(infusion_list)
		return

	assert infusion_dict['infusion_start_time'], "You must specify infusion_start_time."
	assert infusion_dict['infusion_end_time'], "You must specify infusion_end_time."
	assert type(infusion_dict['volume']) == float, "volume must be a number in µl , not a string"
	assert type(infusion_dict['concentration']) == float, "concentration must be a number in µg/µl, not a string."
	assert ignore_sess == 0 and ignore_reason is None, "You must provide a reason to ignore this session."

	experid, expername = get_experid_from_info(experid)

	# take session date from infusion start time
	sessdate = datetime.strptime(infusion_dict['infusion_start_time'], '%Y-%m-%d %H:%M:%S').date()
	cntrl_date = sessdate + timedelta(days=-1)
	sessdate = sessdate.strftime("%Y-%m-%d")
	cntrl_date = cntrl_date.strftime("%Y-%m-%d")

	a = datetime.strptime(infusion_dict['infusion_end_time'], '%Y-%m-%d %H:%M:%S') - datetime.strptime(infusion_dict['infusion_start_time'], '%Y-%m-%d %H:%M:%S')
	infusion_duration = a.seconds / 60

	out = dbc.query('select sessid from beh.sessview where sessiondate = %s and subjid = %s',(sessdate, subjid))
	if not out:
		print("{} hasn't been trained on {} yet. Please log infusion again after the training has started.".format(subjid, sessdate))
		return
	sessid = out[0][0] #just in case this animal is run multiple times in a day
	
	out = dbc.query('select sessid from beh.sessview where sessiondate = %s and subjid = %s',(cntrl_date, subjid))
	if not out:
		print("{} hasn't been trained on {}. cntrl_date and cntrl_sessid will be left blank".format(subjid, cntrl_date))
		cntrl_date = []
		cntrl_sessid = []
	cntrl_sessid = out[0][0]

	# make a commment 
	cmt = '{} µl infusion of {} today by {}. '.format(infusion_dict['volume'], infusion_dict['drug'], infusion_dict['performed_by'])
	add_subject_comment(subjid, cmt, experid, infusion_dict['infusion_start_time'])
	
	infusion_dict['sessid'] = sessid
	infusion_dict['cntrl_sessid'] = cntrl_sessid
	infusion_dict['infusiondate'] = sessdate
	infusion_dict['infusion_duration'] = infusion_duration
	infusion_dict['notes'] = notes
	infusion_dict['ignore_sess'] = ignore_sess
	infusion_dict['ignore_reason'] = ignore_reason
		
	dbc.save('infusions', infusion_dict)

	notify_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	notify('The infusion log of {} was updated on {} by {}'.format(subjid, notify_time, expername))

def log_opto(subjid, date, opto_dict, experid=0, notes=None, ignore_sess=0, ignore_reason=None):
	'''log_opto(subjid, opto_dict, experid=0, notes=None, ignore_sess=0, ignore_reason=None)
		 Input:
		 subjid is subjid, so far list is not supported
		 date: '2020-12-25', the date of opto manipulation
		 opto_dict should contain the following fields:
		 {'region': 'Left FOF', # the region of opto manipulation
		  'opto_fraction': 0.3, # the fraction of opto trials in a session, between 0-1
		  'laser_type': 'green', # the type of laser used
		  'trigger_type': 'single-trigger', # generally choose between single-trigger and state-dependent, but any chars accepted
		  'laser_strength':'9.1', # laser strength measured in mW
		  'performed_by':'chaofei'}
		 experid: your experid or name
		 notes: any comments go here
		 ignore_sess: 0 or 1
		 ignore_reason: if ignore_sess = 1, you must provide ignore_reason
	'''
	opto_list = ['region', 'opto_fraction', 'laser_type', 'trigger_type', 'laser_strength', 'performed_by']
 
	assert opto_dict['region'], "You must specify opto region."
	assert opto_dict['laser_type'], "You must specify laser_type."
	assert opto_dict['trigger_type'], "You must specify trigger_type."
	assert opto_dict['opto_fraction'] > 0 and opto_dict['opto_fraction'] < 1, "opto_fraction must be between 0-1."
	assert type(opto_dict['laser_strength']) == float, "laser_strength must be a float number in mW, e.g. 9.1."
	assert (ignore_sess == 0) == (ignore_reason is None), "You must provide a reason to ignore this session."

	experid, expername = get_experid_from_info(experid)

	out = dbc.query('select sessid from beh.sessview where sessiondate = %s and subjid = %s', (date, subjid))
	if not out:
		print("{} hasn't been trained on {} yet. Please log opto again after the training has started.".format(subjid, date))
		return
	sessid = out[0][0] #just in case this animal is run multiple times in a day
	
	# make a commment 
	cmt = 'Opto manipulation on {} {} on {} by {}. '.format(subjid, opto_dict['region'], date, opto_dict['performed_by'])
	add_subject_comment(subjid, cmt, experid)
	
	opto_dict['sessid'] = sessid
	opto_dict['notes'] = notes
	opto_dict['ignore_sess'] = ignore_sess
	opto_dict['ignore_reason'] = ignore_reason
		
	dbc.save('opto', opto_dict)

	notify_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	notify('The opto log of {} was updated on {} by {}'.format(subjid, notify_time, expername))

def log_injection(subjid, injection_dict, experid=0, comment=None):
	'''def log_injection(subjid, injection_dict, experid=0, comment=None)
		Input:
		subjid is subjid, so far list is not supported
		injection_dict should contain the following fields:
		{'injection_time': "2020-07-10 14:21:00",
		'drug':'ACTH', # the drug name
		'dose': '0.3', # dose in mg/kg,
		'volume': '1.0' # volume in mL
		'method': 'SC', # must be in ('SC','IP','IM','IV','IC')}
		experid: your experid or name
		comment: any comments go here
	'''
	injection_list = ['injection_time', 'drug', 'dose', 'volume', 'method']
	
	if not all(name in injection_dict for name in injection_list):
		print('Your dictionary includes the following names:{}').format(injection_dict.keys())
		print('It must include these names :{}').format(injection_list)
		return

	assert injection_dict['injection_time'], "You must specify injection_time."
	assert type(injection_dict['dose']) == float, "dose must be a float number in mg/kg , not a string"
	assert type(injection_dict['volume']) == float, "volume must be a float number in mL , not a string"
	assert injection_dict['method'] in ['SC','IP', 'IM', 'IV', 'IC'], "Injection method must be in ['SC','IP', 'IM', 'IV', 'IC']"

	experid, expername = get_experid_from_info(experid)

	# take session date from injection_time
	sessdate = datetime.strptime(injection_dict['injection_time'], '%Y-%m-%d %H:%M:%S').date()
	out = dbc.query('select sessid from beh.sessview where sessiondate = %s and subjid = %s',(sessdate, subjid))
	if not out:
		print("{} hasn't been trained on {} yet. Please log injection again after the training has started.".format(subjid, sessdate))
		return
	sessid = out[0][0] #just in case this animal is run multiple times in a day

	# make a commment 
	cmt = '{} mL, {} mg/kg injection of {} today by {}. '.format(injection_dict['volume'], injection_dict['dose'], injection_dict['drug'], expername)
	add_subject_comment(subjid, cmt, experid, injection_dict['injection_time'])
	
	injection_dict['ts'] = injection_dict.pop('injection_time')
	injection_dict['subjid'] = subjid
	injection_dict['sessid'] = sessid
	injection_dict['comment'] = comment
	injection_dict['experid'] = experid
		
	dbc.save('injection_log', injection_dict)

	notify_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	notify('The injection log of {} was updated on {} by {}'.format(subjid, notify_time, expername))

if __name__ == '__main__':
	#pass
	#newegid = duplicate_expgroup('risk002','risk003', group_description='A mouse pipeline with rand1 that only uses the same ports as risky-choice')
	#D = {'species':'mouse', 'strain':'C57B6', 'arrivedage':49,'iacucprotocol':'15-1465','gender':'M','number_of_animals':42}
	#order_animals(D,'Jeff','2017-10-1')
	#sac_rfid(84,"Jeff")
	#sac_subjid(666)
	#sac_subjid(666,"Jeff",cmt="testing to see why things are not updating in DB")
		#set_subject_stage(666, 4, experid='Jeff', clear_subject_settings=True)

	#tag_animal(2151,351)	
	newcage = create_cage(666,'Jeff')
	pass
