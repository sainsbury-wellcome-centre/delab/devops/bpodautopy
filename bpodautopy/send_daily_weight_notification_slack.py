import sys
import os
import datetime
import json
import requests
import configparser
import schedule
import pandas as pd
import traceback as tb
# from time import sleep
# from threading import Thread
from db import Engine, Connection

DBE = Engine()

def push_notification_water_restricted_animals():
    ini_path = os.path.join(os.path.expanduser('~'),'.dbconf')
    config = configparser.ConfigParser()
    config.sections()
    config.read(ini_path)
    slack_url = config['slack']['url']

    sql = 'select b.subjid,b.pyratid,b.pyratcageid,a.mass,b.owner from met.mass a right join met.animals b on (a.subjid = b.subjid and CAST(a.mdate AS DATE) = CAST(curdate() AS DATE)) where b.water = "controlled" and b.status != "dead" and b.species != "tester"'
    df = pd.read_sql_query(sql, DBE)
    df2 = df[df["mass"].isnull()]
    if len(df2)>0:
        message = (df2.to_markdown(index=False))
        message = "```\n" + message + "\n```"
        title_text = ":Warning:Water Restricted Animals :Rat: Not Been Weighted Today! <!channel>"
    else:
        message = " "
        title_text = "All Water Restricted Animals :Rat: Have Been Weighted Today! :tada:"

    slack_data = {
        "username": "NotificationBot",
        "channel": "C04891LMETG",
        "blocks": [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": title_text
                }
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": message,
                }
            }
        ]
    }

    byte_length = str(sys.getsizeof(slack_data))
    headers = {'Content-Type': "application/json", 'Content-Length': byte_length}
    response = requests.post(slack_url, data=json.dumps(slack_data), headers=headers)
    return response

def push_notification_normal_animals():
    ini_path = os.path.join(os.path.expanduser('~'),'.dbconf')
    config = configparser.ConfigParser()
    config.sections()
    config.read(ini_path)
    slack_url = config['slack']['url']

    sql = 'select b.subjid,b.pyratid,b.pyratcageid,a.mass,b.owner from met.mass a right join met.animals b on (a.subjid = b.subjid and CAST(a.mdate AS DATE) > DATE(NOW() - INTERVAL 7 DAY)) where b.water = "free" and b.status != "dead" and b.species != "tester"'
    df = pd.read_sql_query(sql, DBE)
    df2 = df[df["mass"].isnull()]
    if len(df2)>0:
        message = (df2.to_markdown(index=False))
        message = "```\n" + message + "\n```"
        title_text = ":Warning:Free Animals :droplet::Rat: Not Been Weighted Over 7 Days! <!channel>"
    else:
        message = " "
        title_text = "All Free Animals :droplet::Rat: Have Been Weighted Recently! :tada: "

    slack_data = {
        "username": "NotificationBot",
        "channel": "C04891LMETG",
        "blocks": [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": title_text
                }
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": message,
                }
            }
        ]
    }

    byte_length = str(sys.getsizeof(slack_data))
    headers = {'Content-Type': "application/json", 'Content-Length': byte_length}
    response = requests.post(slack_url, data=json.dumps(slack_data), headers=headers)
    return response

if __name__ == "__main__":
    schedule.every().day.at("17:00").do(push_notification_water_restricted_animals)
    schedule.every().day.at("17:01").do(push_notification_normal_animals)
    #push_notification_water_restricted_animals()
    #push_notification_normal_animals()

    while True:
        schedule.run_pending()

