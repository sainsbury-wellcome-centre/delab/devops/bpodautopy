import os
import configparser
import sys
import requests
import json
from sys import getsizeof
from os import path
import traceback

SLACK_SIZE_LIMIT = 2000



def slack_push_notification(title: str, message: str, channel: str, slack_url: str):
    """
    Send any instant push notification to slack
    """


    if not slack_url:
        ini_path = os.path.join(os.path.expanduser('~'),'.dbconf')
        config = configparser.ConfigParser()
        config.sections()
        config.read(ini_path)
        slack_url = config['slack']['url']

    slack_data = None

    if title is None and message is None:
        title = "Notification Sample"

    if title is None and message is not None:
        slack_data = {
            "username": "NotificationBot",
            "channel": channel,
            "blocks": [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": message,
                    }
                }
            ]
        }
    elif message is None:
        slack_data = {
            "username": "NotificationBot",
            "channel": channel,
            "blocks": [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": title
                    }
                }
            ]
        }
    else:
        slack_data = {
            "username": "NotificationBot",
            "channel": channel,
            "blocks": [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": title
                    }
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": message,
                    }
                }
            ]
        }

    try: 
        byte_length = str(sys.getsizeof(slack_data))
        headers = {'Content-Type': "application/json", 'Content-Length': byte_length}
        response = requests.post(slack_url, data=json.dumps(slack_data), headers=headers)

        if response.status_code != 200:
            print("Failed sending the slack message!!")
    except:
        traceback.print_exc()
        print("Failed sending the slack message!!")
        response = 0
    return response


def get_slack_url():
    import configparser
    ini_path = path.join(path.expanduser('~'),'.dbconf')
    config = configparser.ConfigParser()
    config.sections()
    config.read(ini_path)
    return config['slack']['url']


def send_slack_msg(title_text: str, message: str , channel: str, slack_url: str):
    if len(message)>SLACK_SIZE_LIMIT:
        ongoing_message = message
        first_message = True
        while len(ongoing_message)>SLACK_SIZE_LIMIT:
            message_to_print = ongoing_message[0:SLACK_SIZE_LIMIT]
            idx = message_to_print.rfind("<http://") if message_to_print.rfind("<https://") == -1 else message_to_print.rfind("<https://")#to avoid url break
            idx = SLACK_SIZE_LIMIT if idx == -1 else idx
            if first_message:
                slack_push_notification(title_text,ongoing_message[0:idx],channel,slack_url)
            else:
                message = slack_push_notification(None,ongoing_message[0:idx],channel,slack_url)
            ongoing_message = ongoing_message[idx:]
            first_message = False
        if len(ongoing_message)>0:
            message = slack_push_notification(None,ongoing_message,channel,slack_url)
    else:
        message = slack_push_notification(title_text,message,channel,slack_url)

