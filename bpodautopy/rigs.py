from datatable import fread, f 

RIG_STATUS_URL = "http://lab.deneuro.org/conf/rig_status.csv"

def check_session_started(rigid, subjid):
    df = fread(RIG_STATUS_URL)
    df = df[(f.rigid == rigid) & (f.subjid == subjid),:]
    if df.nrows > 0:
        return df[0, "starttime"]
    else:
        return False
df.name