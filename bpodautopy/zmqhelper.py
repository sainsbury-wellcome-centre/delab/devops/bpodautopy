''' 
    A helper class for publishing or subscribing to ZMQ messages.
'''
import zmq
import json
import random
import time


def get_conf(config_section):
     
    import configparser as cp
    config = cp.ConfigParser()
    config.read(defaultConfigFile())
    assert config_section in config.sections(), "There is no section called {}. Check your .dbconf file.".format(config_section)
    return dict(config.items(config_section))

def publisher(config_section='zmq'):
    """
        Return a publish socket attached to our proxy.
    """
    ctx = zmq.Context.instance()
    pub = ctx.socket(zmq.PUB)
    pub.setsockopt(zmq.HEARTBEAT_IVL,60000)
    url = puburl(config_section)
    pub.connect(url)
    return pub

def pusher(config_section='zmq'):
    """
        Return a publish socket attached to our proxy.
    """
    ctx = zmq.Context.instance()
    pub = ctx.socket(zmq.PUSH)
    pub.setsockopt(zmq.HEARTBEAT_IVL,60000)
    url = pushurl(config_section)
    pub.connect(url)
    return pub

def subscriber(topic, config_section='zmq'):
    """
        Return a publish socket attached to our proxy.
    """
    ctx = zmq.Context.instance()
    sub = ctx.socket(zmq.SUB)
    url = suburl(config_section)
    sub.setsockopt(zmq.SUBSCRIBE, bytes(topic,'ascii'))
    sub.setsockopt(zmq.HEARTBEAT_IVL,60000)
    sub.connect(url)
    return sub

def suburl(config_section):
    d = get_conf(config_section)
    return "{}:{}".format(d['url'],d['subport'])

def puburl(config_section):
    d = get_conf(config_section)
    return "{}:{}".format(d['url'],d['pubport'])


def pushurl(config_section):
    d = get_conf(config_section)
    return "{}:{}".format(d['url'],d['pushport'])



def defaultConfigFile():
    import os
    from os.path import expanduser, sep
    return expanduser("~") + sep + u".dbconf"


