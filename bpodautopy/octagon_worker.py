import json
import logging
from logging.handlers import RotatingFileHandler
import re
import socket
from os import path
from db import Connection
from slack_helper import send_slack_msg
from zmqhelper import subscriber


# Code for logging with a limited file size 5Mb

log_formatter = logging.Formatter('%(asctime)s %(levelname)s %(funcName)s(%(lineno)d) %(message)s')
logFile = path.expanduser('~/.octagon_worker.log')
my_handler = RotatingFileHandler(logFile, mode='a', maxBytes=5*1024*1024, 
                                 backupCount=2, encoding=None, delay=0)
my_handler.setFormatter(log_formatter)
my_handler.setLevel(logging.INFO)
app_log = logging.getLogger('octagon_worker')
app_log.setLevel(logging.INFO)
app_log.addHandler(my_handler)



DEBUG = 1
DBC :Connection = None

SLACK_CHANNEL_MSG_TITLE = "Octagon Worker"
SLACK_URL = "https://hooks.slack.com/services/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"
SLACK_CHANNEL = "#octagon-worker"

def diffdict(a:dict, b:dict):
    '''
        B - A of dictionary
    '''
    ka = a.keys()
    kb = b.keys()
    d = {}
    if a == b:
            return d
    for newk in kb:
        if (newk not in ka) or (a[newk] != b[newk]):
            d[newk]=b[newk]
    return d

def eprint(error_msg):
    app_log.error(error_msg)
    if(DEBUG > 0):
        print(error_msg)
        send_slack_msg(None,error_msg , SLACK_CHANNEL,SLACK_URL)

octagon_sess_cache = {}
pseudo_subj_cache = {}

def check_subjid_format(data:dict):
    if 'subjid' in data.keys():
        subjids = data['subjid']
        
        if isinstance(subjids,str):
            subjids = [subjids]
        
        for sub in subjids:
            if not re.match(r"[A-Z][A-Z][A-Z]-[A-Z]-[0-9][0-9][0-9][0-9]",sub):
                return False
    
    return True

def check_subjid_exists(data:dict):
    if 'subjid' in data.keys():
        subjids = data['subjid']

        for sub in subjids:
            if not len(list(DBC.get(f"select subjid from met.animal_info where subjid = '{sub}'"))) > 0:
                return False      
    
    return True

def get_subjid(data:dict):
    assert 'subjid' in data.keys(), "subjid key not in data dist"
    assert check_subjid_exists(data), "subjid does not exist in the database"

    subjids = data['subjid']

    if len(subjids) == 1:
        return subjids[0]

    return getPseudoSubjectID(subjids)

def getPseudoSubjectID(subjids : list):
        """
        Helper function to generate a pseudo subjid. This will get the 
        this pseudo id from the db if present or create a new id.

        to create a new id :-
        - sort the subject ids (subA,subB) & (subB,subA) is same) [VERY IMP]
        - all pseudo subjects have prefix as the experimentor no 1
        - M/R changed to P to represent pseudo subject
        - numerics assigned sequentially (unique) append 1 to the last id [ DB query run to get last/no of rows]
        - the [PseudoSubId,  LookupCode (concat subids)] pushed to the table
        - pseudo subjid returned
            subids : list -> list of subject ids in the trial
        return
            subjid : single pseudo subjid
        """    
        assert len(subjids) > 1, "Only one subjid present. No need to get a pseudo subjid"
        subjids.sort() # mutating the list
        lookupCode = ",".join(subjids)
        pseudo_subid = None
        pseudo_subid_table = None

        # looking in the cache. DB query for every msg not required now
        if lookupCode in pseudo_subj_cache.keys():
            return pseudo_subj_cache[lookupCode]

        #query data base using lookup
        try: 
            pseudo_subid_table = DBC.select(table = "met.pseudo_subject", fields=["subjid"], wherestr=f"subjlist = '{lookupCode}'")
        except Exception as e: 
            eprint(f"Error querying the table to get pseudo id. Error: {str(e)}")
            

        # check if None or no item in dict or subjid not there in keys
        if pseudo_subid_table is None or not bool(pseudo_subid_table) or "subjid" not in pseudo_subid_table.keys():
            #the subject was not found in the db
            exp_person = subjids[0].split("-")[0]
            last_pseudo_subjid = list(DBC.get(f"select max(subjid) from met.pseudo_subject where subjid like '{exp_person}-P%'"))[0][0] 
            new_no = int(last_pseudo_subjid.split('-')[2]) + 1 if last_pseudo_subjid else 1
            subjid = f"{exp_person}-P-{new_no:04}"

            #insert the new pseudo sub

            DBC.execute(f"INSERT INTO met.pseudo_subject (subjid, subjlist) \
                                                VALUES    ('{subjid}', '{lookupCode}')")
            pseudo_subid = subjid
                                                
        else:
            # the key was found in the DB
            pseudo_subid = pseudo_subid_table['subjid'][0]

        # insert in cache for faster lookup
        pseudo_subj_cache[lookupCode] = pseudo_subid
        
        return pseudo_subid

class SessionCache:
    '''
        class used for caching tings from a session
    '''
    rig_state: str = None
    session_id: str = None
    last_trial_num: int = None
    last_trial_settings: dict = None #here to save only changed settings (save space on db)
    
    #used to catch any non recoverable Error in the session in which case all of the session skipped
    faulty_session: bool = False


    def __init__(self,rig_state:str =None ,session_id: str = None, last_trial_num: int = 0):
        self.rig_state = rig_state
        self.session_id = session_id
        self.last_trial_num = last_trial_num
        self.last_trial_settings = {}
        self.faulty_session = False

    @staticmethod
    def get_sessdata_db(rigid: str):
        '''
            populating data for cache from the db

            return: 
            session status, session id
        '''
        fields = ['sessid', 'sess_status']
        # TODO: check for the subjids too
        data_dict = DBC.select('beh.current_sessview' ,fields , wherestr= f"rigid = {rigid}")

        if data_dict is not None:
            if not (set(fields) - data_dict.keys()):
                # the fields are present
                sess_status = data_dict['sess_status'][0]
                sess_id = data_dict['sessid'][0]
                if sess_status != "running":
                    # trying to get the last session from the DB
                    # the current session is not there in the DB
                    return None,None
                return sess_status, sess_id

        # the fields are not present
        # send Error as something went wrong 
        # sess_id not in cache or the DB
        eprint(f"Session status not in cache or DB for {rigid}")
        return None,None
        

    @staticmethod
    def get_last_trial_db(session_id:  str) -> int:

        sqlstr = f'SELECT max(trialnum) FROM beh.trialsview where sessid={session_id}'
        dbc_res = list(DBC.get(sqlstr))[0][0]
        if dbc_res != None:
            last_trial_num = int(dbc_res)
        else:
            last_trial_num = 0
        return last_trial_num
    
    @staticmethod
    def getCachedOctagonRig(rigid:str):

        if rigid in octagon_sess_cache.keys():
            return octagon_sess_cache[rigid]
        else:
            rig_state, session_id = SessionCache.get_sessdata_db(rigid)
            if session_id == None:
                eprint(f"Trying to get session data from the db before it is in the db. Cache error for this rig: {rigid}")
                return None
            last_trial_num  = SessionCache.get_last_trial_db(session_id)
            oct_rig = SessionCache(rig_state, session_id, last_trial_num)
            
            octagon_sess_cache[rigid] = oct_rig
            return oct_rig

    @staticmethod
    def getSessionId(rigid: str):
        '''
            given rigid get session from the cache if present otherwise the db
        '''
        oct = SessionCache.getCachedOctagonRig(rigid)
        if oct != None:
            return oct.session_id
        else:
            return None
    
    @staticmethod
    def getRigState(rigid: str):
        '''
            given rigid get session from the cache if present otherwise the db
        '''
        oct = SessionCache.getCachedOctagonRig(rigid)
        if oct != None:
            return oct.rig_state
        else:
            return None

    @staticmethod
    def getLastTrialNum(rigid: str):
        '''
            given rigid get last trial num from the cache if present otherwise the db
        '''
        oct = SessionCache.getCachedOctagonRig(rigid)
        if oct != None:
            return oct.last_trial_num
        else:
            return None

    @staticmethod
    def getLastSettings(rigid:str):
        # TODO: fall back for last_trial setting. right now put entire again
        oct = SessionCache.getCachedOctagonRig(rigid)
        if oct != None:
            return oct.last_trial_settings
        else:
            return {}
    
    @staticmethod
    def getSessionFaulty(rigid: str):
        '''
            given rigid get if current sess faulty
        '''
        if rigid in octagon_sess_cache.keys():
            return octagon_sess_cache[rigid].faulty_session
        return False
    

class OctagonWorker:

    def __init__(self, msg: str):
        DBC.check_connection()

        self.msg = msg

        self.rigid = None
        self.msgtype = None


    def handle_msg(self):

        app_log.info(f"Starting to process the message: {self.msg}")

        try: 
        #determine start,end,save
            prefix, data_json = self.msg.split(' ', 1)
            rigid , msgtype = prefix.split('_')[0] , prefix.split('_')[1]

            self.rigid = rigid
            self.msgtype = msgtype

            data = json.loads(data_json)
        except Exception:
            raise Exception("Message recv not in the correct format")

        
            
        if 'error' in data.keys():
            # an error has occured on the host machine while starting log and 
            # send a slack message
            eprint(f"Recieved an Error message from octagon : {self.msg}")
        elif "startsession" == msgtype:
            self.start_session(data)
        elif "savetrial" == msgtype:
            self.save_trial(data)
        elif "endsession" == msgtype:
            self.end_session(data)
        
        app_log.info(f"----------Ending to process the message-------------")

    def format_trial_data(self,data:dict):

        if "wall" in data.keys() and not isinstance(data['wall'],list):
            data['wall'] = [data['wall']]

        if not "hit" in data.keys():
            data['hit'] = data['reward'] > 0
        
        if not 'violation' in data.keys():
            data['violation'] = data['RT'] > 28

        if data['violation']:
            data['RT'] = None

    def start_session(self,data :dict):

        try:

            # assert subjid are in the correct format. ignore msg if not in correct format
            assert check_subjid_format(data),f"Got wrong subj id format. Ignoring the message"

            subjid = get_subjid(data)

            if subjid is None:
                eprint(f"Error: Could not generate a pseudo subject id. ZMQ: {self.msg}")
                raise Exception('Pseudo Subjid not generated') #caught in the try,except here

            

            data['subjid'] = subjid

            #check for host IP

            if 'hostip' not in data.keys() or data['hostip'] is None:
                eprint("Warning: No host IP found for the current session")

            # TODO: assert last insert was succesful with caching  
            
            err = DBC.save('beh.sessions',data) 
            if err != 0:
                # exception caught in catch
                raise Exception(f"Failed to save start_session.")

            session_id = DBC.last_insert_id() 

            self.update_status(str(session_id),"running",self.rigid)
            _slack_msg = f"Started Session for rig: {self.rigid}; SessID: {session_id}"
            send_slack_msg(None, _slack_msg, SLACK_CHANNEL,SLACK_URL)

            # here we check if rig id there in cache 
            # if not just insert
            # if present check if last session for this rig ended if not send slack

            if self.rigid in octagon_sess_cache.keys():
                # key is present
                oct_rig_obj = octagon_sess_cache[self.rigid]

                if oct_rig_obj.rig_state != "finished":
                    # last trial on this rig did not finish check this
                    send_slack_msg(None,f"Error: Please check last session for rig:{self.rigid}. last session_id:{oct_rig_obj.session_id}", SLACK_CHANNEL,SLACK_URL)
                
            
            # key not present/flush last one. just inserting to cache, trial_num = 0
            octagon_sess_cache[self.rigid] = SessionCache('running',str(session_id))


        except Exception as e:
            eprint(f"There was an error while starting a session: {str(e)}. ZMQ: {self.msg}")

            # now if there was an error while starting the session
            # need to flush the cache still
            octagon_sess_cache[self.rigid] = SessionCache('error','-1')
            octagon_sess_cache[self.rigid].faulty_session = True

    def save_trial(self, data: dict):
        
        try:
            

            curr_trial_num = int(data['trial_num'])
            last_trial_num = SessionCache.getLastTrialNum(self.rigid)
            if SessionCache.getSessionFaulty(self.rigid):
                # there was a session Errorthat was not recoverable. 
                # skipping subsequentsave trials
                return
            
            # assert subjid are in the correct format. ignore msg if not in correct format
            assert check_subjid_format(data),f"Got wrong subj id format. Ignoring the message"
            

            if curr_trial_num < last_trial_num:
                # this is an error probably stop the entire session
                # SHOULD NEVER HAPPEN (can if start session skipped)
                
                octagon_sess_cache[self.rigid].faulty_session= True
                eprint(f"Error. The entire session being skipped for ZMQ. Trial msg encountered \n {self.msg}")
                return
            assert SessionCache.getRigState(self.rigid)=='running', f"Encounter Trials before sess start for {self.rigid}"
            
            if curr_trial_num - 1 != last_trial_num:
                # skipp this message and send a alck
                eprint(f"Error! Trial num {last_trial_num} was skipped")


            subjid = get_subjid(data)

            if subjid is None:
                raise Exception('Could not generate a pseudo subject id') #caught in catch
            data_trials = {}
            data_trials['subjid'] = subjid
            data['pseudo_subjid'] = subjid

            # get the sessid from the cache

            data_trials['sessid'] = SessionCache.getSessionId(self.rigid)

            if data_trials['sessid'] is None:
                raise Exception("Session Id for the trial was not found")

            # format some of the things in data to accomodate for historical format
            self.format_trial_data(data) #mutates data dict

            data_trials['trialtime'] = data['ts']
            data_trials['trialnum'] = data['trial_num']
            data_trials['hit'] = data['hit']
            data_trials['viol'] = data['violation']
            data_trials['reward']  = data['reward']
            data_trials['RT'] = data['RT']
            # data_trials['n_pokes'] = "null"

            # save data_trials
            err = DBC.save('beh.trials',data_trials) 
            if err != 0:
                raise Exception("Could not save the trial to beh.trials")
            trial_id = DBC.last_insert_id()


            # only put settings if there is a difference
            last_settings = SessionCache.getLastSettings(self.rigid)
            # difference_settings = {}
            # if last_settings != data['settings']:
            #     for diff in dictdiffer.diff(data['settings']):
            #     difference_settings = { k : data['settings'][k] for k in set(data['settings'].items()) - set(last_settings.items()) }

            difference_settings = diffdict(last_settings, data['settings'])
            # save trial settings (only the difference from last trial)
            trial_settings = {'trialid':trial_id}
            if difference_settings:
                trial_settings['settings'] = json.dumps(difference_settings)

            octagon_sess_cache[self.rigid].last_trial_settings = data['settings']

            # this might not be thread safe. because the last_insert_id depends on order of last inserts.
            err =DBC.save('beh.trial_settings', trial_settings)

            if err != 0:
                eprint(f"Could not save the trial to beh.trial_settings. Trial id {trial_id}: {trial_settings}")

            # also dump all data into trial_data
            data.pop('settings',None) # We already saved the settings
            # TODO: Should we remove keys that are already saved in trials?
            trial_data = {}
            trial_data['trialid'] = trial_id
            if data:
                trial_data['data'] = json.dumps(data)
            err = DBC.save('beh.trial_data', trial_data)
            if err != 0:
                eprint(f"Could not save the trial to beh.trial_data. Trial id {trial_id}: {trial_data}")
            else:
                # succesfully inserted the trial in the db
                octagon_sess_cache[self.rigid].last_trial_num = int(data['trial_num'])
            

        except Exception as e:
            eprint(f"Error while saving trial : {str(e)}.\n {self.msg}")

    def end_session(self, data :dict):
        '''
            populate `sess_ended` and `sess_ended_data` table 
        '''

        try:
            if SessionCache.getSessionFaulty(self.rigid):
                # there was a session Errorthat was not recoverable. 
                # skipping subsequentsave trials
                raise Exception(f"Non Recoverable error was ther in the session. End session not being processed. Msg: {self.msg}")

            # assert subjid are in the correct format. ignore msg if not in correct format
            assert check_subjid_format(data),f"Got wrong subj id format. Ignoring the message"

            assert SessionCache.getRigState(self.rigid)=='running', f"Encounter Sess Ended before sess start for {self.rigid}"
            
            data['sessid'] = SessionCache.getSessionId(self.rigid)

            if data['sessid'] is None:
                raise Exception(f"Session Id for the end session was not found. Rig: {self.rigid}")

            # TODO: calculate from the choice history
            # data['bias'] = "null"
            data.pop('choice_history', None)
            data.pop('subjid', None)

            err = DBC.save('beh.sess_ended', data)
            if err != 0:
                raise Exception(f"Not able to end session. Session id {data['sessid']}")

            # update the status in the table and the cache

            curr_session = SessionCache.getCachedOctagonRig(self.rigid)
            curr_session.rig_state = 'finished'
            
            self.update_status(data['sessid'],"finished",self.rigid)

            _slack_msg = f"Session succesfully ended for rig {self.rigid}; Session Id : {data['sessid']}"
            send_slack_msg(None, _slack_msg, SLACK_CHANNEL,SLACK_URL)


        except Exception as e:
            eprint(f"Error in End Session. err: {str(e)}. ZMQ : {self.msg}")

    def update_status(self,sessid:str,status:str, rigid:str):
        update_status_dict = {}
        update_status_dict['sessid'] = sessid
        update_status_dict['status'] = status
        
        DBC.save("beh.sess_status", update_status_dict)

        rig_status_lookup = {'running' : 'running' , 'finished' : 'ready'}

        rig_status = {}
        rig_status['rigid'] = rigid
        rig_status['status'] = rig_status_lookup[status]
        DBC.save('met.rig_status', rig_status)


def worker_thread(msg: str):
    '''
        Function compatible to run blocking io operations
        on seperate threads
    '''
    try: 
        oct_wrkr = OctagonWorker(msg) 
        oct_wrkr.handle_msg()
    except Exception as e:
        eprint(f"Problem with worker thread error: {str(e)}")

def main():
    sub = None
    host = socket.getfqdn()
    try :
        
        _slack_msg = f"The octagon Worker is starting now on the ipaddr:{socket.gethostbyname(host)} ; Hostname: {host}"
        send_slack_msg(None, _slack_msg, SLACK_CHANNEL,SLACK_URL)
        sub  = subscriber('464')

        while True: 
            msg = sub.recv_string()
            # TODO : make this async
            worker_thread(msg) 

    except KeyboardInterrupt:
        eprint(f"Worker stopped by the user on the ipaddr:{socket.gethostbyname(host)} ; Hostname: {host}")
    except Exception as e:
        eprint(str(e))
    finally:
        sub.close()
        DBC.close()


if __name__ == '__main__':

    DBC = Connection('manage')

    main()
