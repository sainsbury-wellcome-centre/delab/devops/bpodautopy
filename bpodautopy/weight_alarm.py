'''
Author: Xuan Wen
Date: 2020-11-02 11:14:21
LastEditTime: 2021-03-09 12:15:55
Update: 2022-10-29 by Jingjie Li to be compatible with the new db in SWC
Description: A script which run onces everyday and check the weight of animals. Get the Z-score of 
the most recent weight and compare with last ten days. Send the alarm if the Z-score is <-2 or >3.

1. check animal subject id which subject status is not 'dead'
2. check the latest weight and calculate z-score
2.1. check 7 days weight change and calculate the slope using linear regression
3. get a list of all weight changing animal
4. check their cage mates, if their cage mates are also in the weight changing animal list, send entire box in the alarm
5. send alarm (We want to send alarm every 7:00am, so that Yingkun can check animal and call vet if nessesary)

#28 improve weight alarm
The weight alarm is great! but some room for improvement:

 TODO: No alarm for free water animal gaining weight
 TODO: No alarm for any animal "slowly" gaining weight

FilePath: /labscripts/weight_alarm_zscore.py
'''

# import database function
import sys
from datetime import date, timedelta
import numpy as np
from sklearn.linear_model import LinearRegression
from decimal import *
from db import Connection
from scipy.stats import pearsonr

DBC = None

REWARD_AMOUNT_THRESDHOLD_RAT = 5000
REWARD_AMOUNT_THRESDHOLD_MOUSE = 500
NEW_WATER_RESTRICTION_ANIMAL_DAY_RANGE = 7

BW_WARNING_THRESHOLD_MOUSE = 0.80
BW_WARNING_THRESHOLD_RAT = 0.90
getcontext().prec = 3

# find all alive animals
def animals_list(dbc):
    subjid_list_alive = dbc.query("select DISTINCT subjid from met.animals where status != 'dead' and status != 'ordered' and status != 'arrived' and water = 'controlled' and species<>'tester'")
    return subjid_list_alive
# print(subjid_list_alive)

def get_slack_userid_from_animal_list(subjlist, dbc):
    if subjlist:
        subj_str = ''
        for subj in subjlist:
            subj_str += ("'"+subj+"',")
        subj_str = f"({subj_str[:-1]})"
        sqlstr = f'''
            select slackuser from met.experimenters 
            where experid in 
            (select DISTINCT experid 
            from met.animals where subjid in {subj_str})
        '''
        out = dbc.query(sqlstr)
        slack_user_list = [i[0] for i in out]
        slack_user_str = ''
        for slack_user in slack_user_list:
            slack_user_str += ("<@"+slack_user+"> ")
    else:
        slack_user_str = ''
    return slack_user_str

def cagemate_checking(dbc,alarm_subjid_list):
    '''
    find cage mates of animals:
        1. find the latest cage id (in case of changing cage)
        2. find its all cage mates (except itself)
    '''
    alarm_cage = []
    for subjid in alarm_subjid_list:
        cageid_out = dbc.query(f"SELECT cageid,pyratcageid FROM met.animals WHERE subjid = '{subjid}'")
        cageid = cageid_out[0][0]
        pyratcageid = cageid_out[0][1]
        cage_mates = dbc.query(f"SELECT subjid FROM met.animals WHERE cageid = {cageid} and subjid != '{subjid}'")
        if cage_mates:
            for cagemate in cage_mates:
                cagemate = cagemate[0]
                if (cagemate in alarm_subjid_list) and (pyratcageid not in alarm_cage):
                    alarm_cage.append(pyratcageid)
    return np.unique(alarm_cage)

def remove_outlier(weight_list):
    '''
    remove outliers:
        remove data points that 3 std away.
        (now we left the wrong data points but added a note to weird data)
    '''
    pass

def save_log(message):
    '''
    save log:
        save message everyday
        Do we need this? Since we can change the date of this code so we can get same info at any time.
    '''
    pass

def is_empty(any_structure):
    if any_structure:
        return False
    else:
        return True

def slowly_changing_checking_lr(dbc,subjid):
    latest_update_date = dbc.query(f"select max(mdate) from met.mass where subjid = '{subjid}'")
    #print(latest_update_date)
    seven_days_before_date = latest_update_date[0][0] - timedelta(days=7)
    #print(seven_days_before_date)
    #print(subjid)
    latest_update_date = latest_update_date[0][0].strftime("%Y-%m-%d") #to str
    seven_days_before_date = seven_days_before_date.strftime("%Y-%m-%d")#to str
    
    seven_days_mass = dbc.query(f"select mass from met.mass where subjid = '{subjid}' and mdate >= '{seven_days_before_date}'")
    x = np.arange(0,len(seven_days_mass),1).reshape((-1,1))
    model = LinearRegression().fit(x, seven_days_mass)
    avg_mass = np.mean(seven_days_mass)
    
    # print(subjid)
    # print(seven_days_mass)
    # print("slope: ", model.coef_)
    # print("Avg mass: ", avg_mass)
    # print("Slope/Mass: ", ratio)
    # print("-------------------")
    
    ratio = Decimal(model.coef_[0][0])/Decimal(avg_mass)
    return ratio

def zscore_checking(dbc,subjid):
    # get the latest date of weighting, get the 10 days date before
    latest_update_date = dbc.query(f"select max(mdate) from met.mass where subjid = '{subjid}'")
    ten_days_before = latest_update_date[0][0] - timedelta(days=10)
    latest_update_date = latest_update_date[0][0].strftime("%Y-%m-%d") #to str
    ten_days_before = ten_days_before.strftime("%Y-%m-%d")#to str
    
    #get latest and 10 days mass
    ten_days_mass_list = dbc.query(f"select mass from met.mass where subjid = '{subjid}' and mdate >= '{ten_days_before}' order by mdate desc")
    latest_mass = ten_days_mass_list[0][0]
    # calcualte z-score

    mass_std = np.std(ten_days_mass_list)
    if mass_std > 0:
        z_score = (latest_mass - np.mean(ten_days_mass_list))/np.std(ten_days_mass_list)
        fraction_loss = np.abs(latest_mass - np.mean(ten_days_mass_list))/np.mean(ten_days_mass_list)
    else:
        z_score,fraction_loss = 0,0
    
    # print("Avg = ", np.mean(ten_days_mass_list))
    # print("Std = ", np.std(ten_days_mass_list))   
    # print("Z-score = ",z_score)
    # print("---------------------")
    
    return z_score,fraction_loss

def mass_alarm_slowly_changing_lr(dbc,subjid_list_alive):
    """
    find animals that slowly dropping weight
    return:
        alarm_animal: animals that need to be alerted
        weird_alarm_animal: animals that need to be alerted more
        slope_list: slope of each animal (the data that need to be reported)
        
    """
    alarm_animal = []
    weird_alarm_animal = []
    slope_list = []
    for subjid in subjid_list_alive:
        subjid = subjid[0]
        slope = slowly_changing_checking_lr(dbc,subjid)
        # animals that slowly dropping weight
        if slope < -0.01:
            alarm_animal.append(subjid)
            slope_list.append(slope)
        if slope < -0.03:
            weird_alarm_animal.append(subjid)
    # print("Subject ID of animals need to be alerted: ",alarm_animal)
    return alarm_animal, weird_alarm_animal, slope_list

def get_mass_list(dbc,subjid, n_days):
    '''
    get mass list of n days before
    '''
    if dbc == None:
        dbc = Connection()
    dbc.check_connection()

    latest_update_date = dbc.query(f"select max(mdate) from met.mass where subjid = '{subjid}'")
    n_days_before_date = latest_update_date[0][0] - timedelta(days=n_days)
    latest_update_date = latest_update_date[0][0].strftime("%Y-%m-%d") #to str
    n_days_before_date = n_days_before_date.strftime("%Y-%m-%d")#to str
    n_days_mass = dbc.query(f"select mass from met.mass where subjid = '{subjid}' and mdate >= '{n_days_before_date}'")
    days_list = np.arange(0,len(n_days_mass),1)
    mass_list = np.array(n_days_mass).reshape(-1).astype(float)
    return days_list , mass_list

def slowly_changing_checking_corr(dbc,subjid):
    
    days_list, mass_list = get_mass_list(dbc,subjid,10)
    r, p = pearsonr(days_list, mass_list)
    
    return r,p


def mass_alarm_slowly_changing_corr(dbc,subjid_list_alive):
    """
    find animals that slowly dropping weight
    return:
        alarm_animal: animals that need to be alerted
        weird_alarm_animal: animals that need to be alerted more
        slope_list: slope of each animal (the data that need to be reported)
        
    """
    alarm_animal = []
    weird_alarm_animal = []
    corr_list = []
    for subjid in subjid_list_alive:
        subjid = subjid[0]
        r,p = slowly_changing_checking_corr(dbc,subjid)
        # animals that slowly dropping weight
        if p < 0.01 and r < 0:
            alarm_animal.append(subjid)
            corr_list.append(r)
        if p < 0.01 and r < -0.9:
            weird_alarm_animal.append(subjid)
    # print("Subject ID of animals need to be alerted: ",alarm_animal)
    return alarm_animal, weird_alarm_animal, corr_list



def mass_alarm_zsore(dbc,subjid_list_alive):
    alarm_animal = []
    weird_alarm_animal = []
    # free_water_animals = con.query("select subjid from met.animals where water = 'free' and subjid > 1000 and status != 'dead' ")
    zsore_list = []
    
    for subjid in subjid_list_alive:
        subjid = subjid[0]
        z_score,frac_loss = zscore_checking(dbc,subjid)
        
        # exclude free water animals
        no_data_for_water = is_empty(dbc.query(f"select water = 'free' from met.animals where subjid = '{subjid}'")) 
        is_free = 0
        if (not no_data_for_water):
            is_free = dbc.query(f"select water = 'free' from met.animals where subjid = '{subjid}'")[0][0]
        # contains data for water, it's free water, and body weight increases, then ignore it
        if (not no_data_for_water) and is_free and z_score > 0:
            continue
        # animals that quickly dropping weights, but exclude free water animals
        if (z_score > 3 or z_score < -2) and  frac_loss > 0.02:
            alarm_animal.append(subjid)
            zsore_list.append(z_score)
        if (z_score > 4 or z_score < -3) and  frac_loss > 0.02:
            weird_alarm_animal.append(subjid)

    # print("Subject ID of animals need to be alerted: ",alarm_animal)
    return alarm_animal, weird_alarm_animal, zsore_list

def mass_alarm_low_body_weight(dbc):
    sql = f'''
        select 
        a.subjid as subjid,
        a.weight_percentage as weight_percentage 
        from met.today_mass_summary a join met.animal_info b on (a.subjid=b.subjid)
        where (b.species = 'rat' and a.weight_percentage<{BW_WARNING_THRESHOLD_RAT:.2f}) or (b.species='mouse' and a.weight_percentage<{BW_WARNING_THRESHOLD_MOUSE:.2f})
    '''
    result = dbc.query(sql)
    subj_list = []
    weight_percentage_list = []
    for row in result:
        subj_list.append(row[0])
        weight_percentage_list.append(row[1])
    return subj_list, weight_percentage_list

def mass_alarm_low_reward(dbc):
    sqlstr = f'''
        select subjid,total_reward 
        from met.today_mass_summary 
        where total_reward < {REWARD_AMOUNT_THRESDHOLD_RAT:.0f} and baseline>70
    '''
    result = dbc.query(sqlstr)
    subj_list = []
    reward_list = []
    for row in result:
        subj_list.append(row[0])
        reward_list.append(row[1])
    return subj_list, reward_list



        

def add_emoji(msg_type,subj_id_list, subj_data_list,reward_amount,newly_starter_list):
    '''
    add mattermost supported emoji to the alarm
        msg_type: 
            0: slowly weight changing alarm, 
            1: quickly weight changing alarm
    '''
    
    new_link = ""
    
    for index in range(len(subj_id_list)):
        subjid = str(subj_id_list[index])
        extra_info = ""
        if subjid in newly_starter_list:
            extra_info = ":new:"
        
            
        if reward_amount[index] is not None:
            if (subjid[4] == 'R' and reward_amount[index] < REWARD_AMOUNT_THRESDHOLD_RAT) or (subjid[4] == 'M' and reward_amount[index] < REWARD_AMOUNT_THRESDHOLD_MOUSE):
                extra_info = extra_info + f"*{reward_amount[index]:.0f}uL*:weary:"
            else:
                extra_info = extra_info + f"{reward_amount[index]:.0f}uL"
        this_link = f"<http://dash.deneuro.org/{subjid}|{subjid}>"
         #disable link for weight plotting for now since we don't have that yet
        if msg_type == 'slowly-weight-change-lr':
            if subj_data_list[index] >= 0:
                new_link = f"{new_link}{this_link}:chart_with_upwards_trend:{extra_info} Slope = {subj_data_list[index]:.4f}\n"
            else:
                new_link = f"{new_link}{this_link}:chart_with_downwards_trend:{extra_info} Slope = {subj_data_list[index]:.4f}\n"
        elif msg_type == 'slowly-weight-change-corr':
            
            # query the weights here as the only message that needs this info is the the corr message
            # move this out of if statement if required elsewhere
            days_list , mass_list = get_mass_list(DBC,subjid, 10)

            if subj_data_list[index] >= 0:
                new_link = f"{new_link}{this_link}:chart_with_upwards_trend:{extra_info} Corr = {subj_data_list[index]:.2f}"
            else:
                new_link = f"{new_link}{this_link}:chart_with_downwards_trend:{extra_info} Corr = {subj_data_list[index]:.2f}"

            if mass_list is not None and len(mass_list) > 0:
                new_link = new_link + f" | Changed {(100*(mass_list[0] - mass_list[-1])/ mass_list[0] ):.2f}% in 10 days \n"
            else:
                new_link = new_link + "\n"

        # fast change
        elif msg_type == 'quickly-weight-change': 
            if subj_data_list[index] >= 0:
                new_link = f"{new_link}{this_link}:arrow_heading_up:{extra_info} Z-score = {subj_data_list[index]:2f}\n"
            else:
                new_link = f"{new_link}{this_link}:arrow_heading_down:{extra_info} Z-score = {subj_data_list[index]:2f}\n"
        elif msg_type == 'low-bw':
            new_link = f"{new_link}{this_link}:scales:{extra_info} Weight Perc = {subj_data_list[index]}%\n"
        elif msg_type == 'low-reward':
            new_link = f"{new_link}{this_link}:low_battery:{extra_info} Reward = {subj_data_list[index]:.0f}uL\n"
    return new_link

def get_amount_of_water_received_today(dbc,subjlist):
    if subjlist:
        subj_str = ''
        for subj in subjlist:
            subj_str += ("'"+subj+"',")
        subj_str = f"({subj_str[:-1]})"
        sqlstr = f"select subjid,total_reward from met.today_mass_summary where subjid in {subj_str}"
        out = dbc.query(sqlstr)
        out_subjid = [i[0] for i in out]
        out_reward_amount = [i[1] for i in out]

        reward_amount_list = [None] * len(subjlist)
        for index in range(len(subjlist)):
            subjid = str(subjlist[index])
            # exist in the output list?
            if subjid in out_subjid:
                reward_amount_list[index] = out_reward_amount[out_subjid.index(subjid)]
    else:
        reward_amount_list = subjlist
    
    return reward_amount_list
            
def get_new_water_restriction_animal(dbc):
    sqlstr = f"select subjid from met.current_animal_water where status = 'controlled' and CAST(ts AS DATE) > DATE(NOW() - INTERVAL {NEW_WATER_RESTRICTION_ANIMAL_DAY_RANGE:.0f} DAY)"
    out = dbc.query(sqlstr)
    if out:
        out_subjid = [i[0] for i in out]
    else:
        out_subjid = []
    return out_subjid

def orgnize_info_in_markdown(dbc):
    '''
    Main Function:
    Run all other functions to get subject id of animals need to be alarmed.
    Return a message that put all information in a markdown format
    '''
    subjid_list_alive = animals_list(dbc)
    subjid_list_newly_started = get_new_water_restriction_animal(dbc)
    # calculate slope for each animal
    slowly_changing_animal, weird_weight_slowly_change_animal, slope_value = mass_alarm_slowly_changing_lr(dbc,subjid_list_alive)
    reward_amount_slowly_changing_animal = get_amount_of_water_received_today(dbc,slowly_changing_animal)
    # calculate correlation for each animal
    slowly_changing_animal_corr, weird_weight_slowly_change_animal_corr, corr_value = mass_alarm_slowly_changing_corr(dbc,subjid_list_alive)
    reward_amount_slowly_changing_animal_corr = get_amount_of_water_received_today(dbc,slowly_changing_animal_corr)

    # calculate z-score for each animal
    zscore_animal, weird_weight_zscore_animal, zscore_value = mass_alarm_zsore(dbc,subjid_list_alive)
    reward_amount_zscore_animal = get_amount_of_water_received_today(dbc,zscore_animal)
    # check for low body weight animals
    low_bw_animal, low_bw_value = mass_alarm_low_body_weight(dbc)
    reward_amount_low_bw_animal = get_amount_of_water_received_today(dbc,low_bw_animal)
    # check for low reward animals
    low_reward_animal, low_reward_value = mass_alarm_low_reward(dbc)
    reward_amount_low_reward_animal = get_amount_of_water_received_today(dbc,low_reward_animal)
    # check for cage alarm
    cage_alarm =  cagemate_checking(dbc, list(set(slowly_changing_animal) | set(zscore_animal))  )
    #message = "### Animal Weight Alarm :mouse2: \n"
    
    # combine all sub lists into one list
    all_animal_list = list(set(slowly_changing_animal) | set(zscore_animal) | set(slowly_changing_animal_corr) | set(low_bw_animal) | set(low_reward_animal) | set(weird_weight_slowly_change_animal) | set (weird_weight_slowly_change_animal_corr))
    # get the slack user id from animal list
    slack_user_id_str = get_slack_userid_from_animal_list(all_animal_list,dbc)

    
    message = "Today's date: " + date.today().strftime("%Y-%m-%d") + "  \n"
    # add all slack_user_id s to message
    if slack_user_id_str:
        message += f"Attention:{slack_user_id_str} \n "

    if slowly_changing_animal:
        message += "Slowly weight changing animal" +" :exclamation: \n"
        message += add_emoji('slowly-weight-change-lr',slowly_changing_animal, slope_value,reward_amount_slowly_changing_animal,subjid_list_newly_started)
    if slowly_changing_animal_corr:
        message += "Slowly weight changing animal (Correlation check):" + " :exclamation: \n"
        message += add_emoji('slowly-weight-change-corr',slowly_changing_animal_corr, corr_value,reward_amount_slowly_changing_animal_corr,subjid_list_newly_started)
    
    if zscore_animal:
        message += "Quickly weight changing animal(z-score check):" + " :bangbang: \n"
        message += add_emoji('quickly-weight-change',zscore_animal, zscore_value,reward_amount_zscore_animal,subjid_list_newly_started)
    
    if low_bw_animal:
        message += f":warning: LOW BW animals! please check/watering! (Threshold: Rat:{round(BW_WARNING_THRESHOLD_RAT*100)}% Mouse:{round(BW_WARNING_THRESHOLD_MOUSE  *100)}% ) \n"
        message += add_emoji('low-bw',low_bw_animal, low_bw_value,reward_amount_low_bw_animal,subjid_list_newly_started)

    if low_reward_animal:
        message += f":low_battery: Animals did not get enough water in training today! \n"
        message += add_emoji('low-reward',low_reward_animal, low_reward_value,reward_amount_low_reward_animal,subjid_list_newly_started)
    
    if len(cage_alarm)>0:
        message += "Cage contains more than one worrying animal:" +" :fire: \n"+ ",".join(cage_alarm) + "  \n"

    
    
    weird_weight_slowly_change_animal = [x for x in weird_weight_slowly_change_animal if x not in subjid_list_newly_started]
    weird_weight_zscore_animal = [x for x in weird_weight_zscore_animal if x not in subjid_list_newly_started]
    if weird_weight_slowly_change_animal or weird_weight_zscore_animal:
        message += "Likely bad data: :thinking_face: \n"
        if weird_weight_slowly_change_animal:
            message += ",".join([str(int) for int in weird_weight_slowly_change_animal]) + " \n"
        if weird_weight_zscore_animal:
            message += ",".join([str(int) for int in weird_weight_zscore_animal]) + " \n"
    
    if not slowly_changing_animal and not zscore_animal and not cage_alarm and not slowly_changing_animal_corr and not low_bw_animal and not low_reward_animal:
        message += "No animals today have dramatic weight changes. :grin: "
    
    return message

def main_func(dbc):
    message = orgnize_info_in_markdown(dbc)
    print(message)
    #net.sendmattermost('#lab-notifications', message)
    pass


if __name__ == "__main__":
    # # Set up time to run this program (cannot run properly)
    # schedule.clear()
    # schedule.every(1).days.at("07:00").do(main_func)
    # schedule.every(1).days.at("15:00").do(main_func)
    # while True:
    #     schedule.run_pending()
    #     time.sleep(1)
    #     print(datetime.now().strftime("Running Alarm at 7am&15pm; No Interrupting %H:%M:%S"))
    DBC = Connection()
    main_func(DBC)
    DBC.close()