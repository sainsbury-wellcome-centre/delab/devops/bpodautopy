'''
The scheduler should run on the server which has:-
- ogma mounted on it. Check the instructions on how to mount ogma on the server 
  https://gitlab.com/sainsbury-wellcome-centre/delab/hardware/-/wikis/Computing

- the server should have access to the database. db.deneuro.org
- access to the net for slack notifications
- access to zmq services for realtime updates to/from the db

'''


import schedule
from db import Engine, Connection

import time

import scheduling.animal_weight_notif as animal_weight
import scheduling.cage_writer as cage_writer
import scheduling.octagon_rt_writer as octagon_rt_writer
import scheduling.schedule_writer as schedule_writer
import scheduling.session_status_writer as session_status_writer
import scheduling.schedule_change_notif as schedule_change_notif
from scheduling.monitor_db import monitor_db


DBE = Engine()
DBC = Connection()


if __name__ == '__main__':

    schedule.every(1).second.do(session_status_writer.session_status_writer,dbe = DBE)
    schedule.every(3).second.do(cage_writer.current_cage_water_writer,dbe = DBE)
    schedule.every(5).second.do(cage_writer.current_animal_cage_writer,dbe = DBE)
    schedule.every(10).minutes.do(animal_weight.push_notification_water_restricted_animals_done,dbe = DBE, dbc = DBC)
    schedule.every(10).minutes.do(animal_weight.check_realtime_free_animals_weight_upload,dbe = DBE, dbc = DBC)
    schedule.every().day.at("09:00").do(animal_weight.push_notification_normal_animals,dbe = DBE, dbc = DBC)
    schedule.every().day.at("09:00").do(schedule_change_notif.run_compare,dbe = DBE)
    schedule.every().day.at("17:00").do(animal_weight.push_notification_if_missing_water_restricted_animals_today,dbe = DBE, dbc = DBC)
    schedule.every().day.at("23:00").do(schedule_writer.copy_schedule, dbc = DBC)
    schedule.every().day.at("15:55").do(animal_weight.reset_weight_notify_status) #we only send weight completion notification after 4pm, waiting for training to be finished
    # schedule.every().day.at("20:00").do(octagon_rt_writer.export_octagon_RT)
    # write RTs manually

    #schedule the db monitor at 11 every night 
    schedule.every().day.at("23:00").do(monitor_db)

    #schedule pyrat weight for every week on friday at 6am
    schedule.every().friday.at("06:00").do(animal_weight.weekly_weight_csv_for_pyrat_writer,dbe = DBE)
    schedule.every().friday.at("06:01").do(animal_weight.push_notif_weekly_pyrat_weights,dbe = DBE)

    
    while True:
        schedule.run_pending()
        time.sleep(1) # prevent 100% CPU usage
